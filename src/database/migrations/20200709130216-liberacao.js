module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("liberacaos", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      id_contrato: {
        type: Sequelize.INTEGER,
        references: { model: "contratos", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_financeiro: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_colaborador: {
        type: Sequelize.INTEGER,
        references: { model: "colaboradores", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_preliberacao: {
        type: Sequelize.INTEGER,
        references: { model: "preliberacoes", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      colaborador: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      posto: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      rubrica: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ano_retirada: {
        type: Sequelize.STRING,
      },
      inicio_per_aquisitivo: {
        type: Sequelize.STRING,
      },
      fim_per_aquisitivo: {
        type: Sequelize.STRING,
      },
      decimo_terceiro: {
        type: Sequelize.FLOAT
      },
      ferias: {
        type: Sequelize.FLOAT
      },
      encargos: {
        type: Sequelize.FLOAT
      },
      multa_fgts: {
        type: Sequelize.FLOAT
      },
      subtotal: {
        type: Sequelize.FLOAT
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("liberacaos");
  },
};
