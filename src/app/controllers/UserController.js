import * as Yup from 'yup';
import { Op } from 'sequelize';
import User from '../models/User';
import writeLog from '../../utils/logger';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      matricula: Yup.string().required(),
      email: Yup.string().email().required(),
      type: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    if (!(req.userType === 'admin')) {
      return res.status(403).json({
        message:
          'Operação não permitida, o usuário não tem permissão de administrador',
      });
    }

    try {
      const { email } = req.body;

      const userExists = await User.findOne({ where: { email } });

      if (userExists) {
        return res
          .status(422)
          .json({ message: 'O usuário já está cadastrado.' });
      }

      const randomPass = Math.floor(Math.random() * 899999 + 100000);

      req.body.password = randomPass.toString();
      req.body.type = req.body.type.toLowerCase();

      const {
        id,
        matricula,
        name,
        type,
        password,
        new_user,
      } = await User.create(req.body);

      return res.json({ id, matricula, name, email, type, password, new_user });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { email } = req.body;

    try {
      User.update(req.body, { where: { email } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    try {
      const tableLength = await User.count();

      const { page = 1, number = 10 } = req.query;
      const user = await User.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
      });

      let pageNumber = Math.ceil(tableLength / number);

      return res.json({ pages: pageNumber, data: user });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async list(req, res) {
    try {
      const user = await User.findAll({
        where: {
          [Op.or]: [{ type: 'Fiscal' }, { type: 'Gestor' }],
        },
      });

      user.sort(function (a, b) {
        let textA = a.name.toUpperCase();
        let textB = b.name.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json({ data: user });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new UserController();
