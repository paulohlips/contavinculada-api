import fs from 'fs';
import { format } from 'date-fns';

import Liberacao from '../models/Liberacao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';

let PizZip = require('pizzip');
let Docxtemplater = require('docxtemplater');
let path = require('path');

class CreatePdfRescisaoService {
  async store(req, res) {
    const { id_contrato } = req.params;

    const data = await Liberacao.findAll({
      where: {
        id_contrato,
        rubrica: 'rescisao',
      },
      raw: true,
    });

    if (!data.length) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Liberação - Rescisão</h3>
      <p>Não há liberações de Rescisão para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    const {
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      dadosEmpresa,
    } = await Contrato.findOne({
      where: { id: data[0].id_contrato },
      include: [
        {
          model: Empresa,
          as: 'dadosEmpresa',
        },
      ],
    });

    // formatação , .
    function numberToReal(numero) {
      try {
        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
      } catch (error) {
        console.log(error);
      }
    }

    // soma totais gerais
    let total_geral = 0;
    let total_ferias = 0;
    let total_encargos = 0;
    let total_13 = 0;
    let total_fgts = 0;
    let total_remuneracao = 0;

    for (let index = 0; index < data.length; index++) {
      const colaborador = await Colaborador.findOne({
        where: { id: data[index].id_colaborador },
        include: [
          {
            model: Cargo,
            as: 'dadosCargo',
          },
        ],
      });

      data[index].remuneracao = numberToReal(
        colaborador.dadosCargo.remuneracao,
      );
      data[index].posto = colaborador.dadosCargo.nome_cargo;
      total_remuneracao += colaborador.dadosCargo.remuneracao;
    }

    data.forEach(async liberacao => {
      total_geral += liberacao.subtotal;
      total_ferias += liberacao.ferias;
      total_encargos += liberacao.encargos;
      total_13 += liberacao.decimo_terceiro;
      total_fgts += liberacao.multa_fgts;

      liberacao.ferias = numberToReal(liberacao.ferias);
      liberacao.encargos = numberToReal(liberacao.encargos);
      liberacao.subtotal = numberToReal(liberacao.subtotal);
      liberacao.decimo_terceiro = numberToReal(liberacao.decimo_terceiro);
      liberacao.multa_fgts = numberToReal(liberacao.multa_fgts);
    });

    total_ferias = numberToReal(total_ferias);
    total_encargos = numberToReal(total_encargos);
    total_geral = numberToReal(total_geral);
    total_13 = numberToReal(total_13);
    total_fgts = numberToReal(total_fgts);
    total_remuneracao = numberToReal(total_remuneracao);

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      console.log(JSON.stringify({ error }, replaceErrors));

      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
        console.log('errorMessages', errorMessages);
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_liberacao-rescisao.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    // coloca a lista em ordem alfabética
    data.sort(function (a, b) {
      let textA = a.colaborador.toUpperCase();
      let textB = b.colaborador.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    // adiciona o índices
    let index = 1;
    data.forEach(item => {
      item.index = index++;
    });

    // objeto para popular o doc
    doc.setData({
      data,
      total_geral,
      total_ferias,
      total_encargos,
      total_13,
      total_fgts,
      total_remuneracao,
      empresa: dadosEmpresa.name,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `liberacao-rescisao-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdfRescisaoService();
