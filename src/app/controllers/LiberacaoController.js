import * as Yup from 'yup';
import { parseISO, getYear } from 'date-fns';

import Liberacao from '../models/Liberacao';
import Contrato from '../models/Contrato';
import PreLiberacao from '../models/PreLiberacao';
import writeLog from '../../utils/logger';

class LiberacaoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      id_contrato: Yup.number().required(),
      id_financeiro: Yup.number().required(),
      rubrica: Yup.string().required(),
      data: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    try {
      const { id_contrato, id_financeiro, rubrica, data } = req.body;

      const contratoExists = await Contrato.findOne({
        where: { id: id_contrato },
      });

      if (!contratoExists) {
        return res
          .status(422)
          .json({ message: 'O contrato selecionado não está ativo.' });
      }

      const liberacoes = data.map(item => ({
        id_preliberacao: item.id_preliberacao,
        id_financeiro,
        id_contrato,
        id_colaborador: item.id_colaborador,
        rubrica,
        colaborador: item.colaborador,
        posto: item.posto,
        ano_retirada: item.ano_retirada,
        inicio_per_aquisitivo: item.inicio_per_aquisitivo,
        fim_per_aquisitivo: item.fim_per_aquisitivo,
        decimo_terceiro: item.decimo_terceiro,
        ferias: item.ferias,
        encargos: item.encargos,
        multa_fgts: item.multa_fgts,
        subtotal: item.subtotal,
      }));

      liberacoes.forEach(async element => {
        const {
          id_colaborador,
          rubrica,
          ano_retirada,
          inicio_per_aquisitivo,
          fim_per_aquisitivo,
          id_preliberacao,
        } = element;
        const { id, status } = await PreLiberacao.findOne({
          where: {
            id_contrato,
            id: id_preliberacao,
            rubrica,
            id_colaborador,
            ano_retirada,
            inicio_per_aquisitivo,
            fim_per_aquisitivo,
          },
        });

        if (!status) {
          await PreLiberacao.update({ status: true }, { where: { id } });
          await Liberacao.create(element);
        }
      });

      return res.json({
        message: 'Liberações realizadas com sucesso.',
      });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    try {
      await Liberacao.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async cancel(req, res) {
    const { id, id_preliberacao } = req.body;

    try {
      await PreLiberacao.update(
        { status: false },
        { where: { id: id_preliberacao } },
      );

      await Liberacao.destroy({ where: { id }, truncate: true });

      return res.json({ message: 'Exclusão realizada com sucesso' });
    } catch (err) {
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async list(req, res) {
    try {
      const tableLength = await Liberacao.count();

      let { id_contrato, rubrica } = req.params;

      let { page = 1, number = 100, ano, inicio, fim } = req.query;

      if (!ano && inicio && fim) {
        ano = null;
        inicio = getYear(parseISO(inicio)).toString();
        fim = getYear(parseISO(fim)).toString();
      } else if (ano && !inicio && !fim) {
        ano = getYear(parseISO(ano)).toString();
        inicio = null;
        fim = null;
      } else {
        ano = null;
        inicio = null;
        fim = null;
      }

      let liberacoes = await Liberacao.findAll({
        limit: number,
        offset: (page - 1) * number,
        where: {
          id_contrato,
          rubrica,
          ano_retirada: ano,
          inicio_per_aquisitivo: inicio,
          fim_per_aquisitivo: fim,
        },
        order: [['created_at', 'DESC']],
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
      });

      let pageNumber = Math.ceil(tableLength / number);

      if (!(id_contrato && rubrica)) {
        return res
          .status(400)
          .json({ message: 'Contrato, rubrica e ano base são obrigatórios.' });
      }

      let obj13salario = {};
      let objFerias = {};
      let objRescisao = {};

      liberacoes.forEach(item => {
        if (item.rubrica == '13salario') {
          if (
            !obj13salario[
              `${item.dadosContrato.numero_contrato}-13salario-${item.ano_retirada}-${item.id_colaborador}`
            ]
          ) {
            obj13salario[
              `${item.dadosContrato.numero_contrato}-13salario-${item.ano_retirada}-${item.id_colaborador}`
            ] = {
              id_contrato: item.id_contrato,
              ano_retirada: item.ano_retirada,
              colaborador: item.colaborador,
              rubrica: item.rubrica,
              dadosContrato: {
                numero_contrato: item.dadosContrato.numero_contrato,
              },
              subtotal: item.subtotal,
            };
          } else if (
            obj13salario[
              `${item.dadosContrato.numero_contrato}-13salario-${item.ano_retirada}-${item.id_colaborador}`
            ]
          ) {
            obj13salario[
              `${item.dadosContrato.numero_contrato}-13salario-${item.ano_retirada}-${item.id_colaborador}`
            ].subtotal =
              obj13salario[
                `${item.dadosContrato.numero_contrato}-13salario-${item.ano_retirada}-${item.id_colaborador}`
              ].subtotal + item.subtotal;
          }
        } else if (item.rubrica == 'ferias') {
          if (
            !objFerias[
              `${item.dadosContrato.numero_contrato}-ferias-${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}-${item.id_colaborador}`
            ]
          ) {
            objFerias[
              `${item.dadosContrato.numero_contrato}-ferias-${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}-${item.id_colaborador}`
            ] = {
              id_contrato: item.id_contrato,
              inicio: item.inicio_per_aquisitivo,
              rubrica: item.rubrica,
              colaborador: item.colaborador,
              fim: item.fim_per_aquisitivo,
              contrato: item.dadosContrato.numero_contrato,
              subtotal: item.subtotal,
              dadosContrato: {
                numero_contrato: item.dadosContrato.numero_contrato,
              },
            };
          } else if (
            objFerias[
              `${item.dadosContrato.numero_contrato}-ferias-${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}-${item.id_colaborador}`
            ]
          ) {
            objFerias[
              `${item.dadosContrato.numero_contrato}-ferias-${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}-${item.id_colaborador}`
            ].subtotal =
              objFerias[
                `${item.dadosContrato.numero_contrato}-ferias-${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}-${item.id_colaborador}`
              ].subtotal + item.subtotal;
          }
        } else if (item.rubrica == 'rescisao') {
          if (
            !objRescisao[
              `${item.dadosContrato.numero_contrato}-rescisao-${item.id_colaborador}`
            ]
          ) {
            objRescisao[
              `${item.dadosContrato.numero_contrato}-rescisao-${item.id_colaborador}`
            ] = {
              id_contrato: item.id_contrato,
              rubrica: item.rubrica,
              colaborador: item.colaborador,

              contrato: item.dadosContrato.numero_contrato,
              subtotal: item.subtotal,
              dadosContrato: {
                numero_contrato: item.dadosContrato.numero_contrato,
              },
            };
          } else if (
            objRescisao[
              `${item.dadosContrato.numero_contrato}-rescisao-${item.id_colaborador}`
            ]
          ) {
            objRescisao[
              `${item.dadosContrato.numero_contrato}-rescisao-${item.id_colaborador}`
            ].subtotal =
              objRescisao[
                `${item.dadosContrato.numero_contrato}-rescisao-${item.id_colaborador}`
              ].subtotal + item.subtotal;
          }
        }
      });

      objFerias = Object.values(objFerias);
      obj13salario = Object.values(obj13salario);
      objRescisao = Object.values(objRescisao);

      if (objFerias.length !== 0) {
        liberacoes = objFerias;
      }
      if (obj13salario.length !== 0) {
        liberacoes = obj13salario;
      }
      if (objRescisao.length !== 0) {
        liberacoes = objRescisao;
      }

      liberacoes.forEach(item => {
        item.subtotal = parseFloat(item.subtotal.toFixed(2));
      });

      // coloca a lista em ordem alfabética
      liberacoes.sort(function (a, b) {
        let textA = a.colaborador.toUpperCase();
        let textB = b.colaborador.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json({ pages: pageNumber, liberacoes });
    } catch (err) {
      // writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async index(req, res) {
    try {
      const tableLength = await Liberacao.count();

      const { page = 1, number = 10 } = req.query;
      const liberacoes = await Liberacao.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
        where: { status: false },
      });

      let pageNumber = Math.ceil(tableLength / number);

      return res.json({ pages: pageNumber, data: liberacoes });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }
}

export default new LiberacaoController();
