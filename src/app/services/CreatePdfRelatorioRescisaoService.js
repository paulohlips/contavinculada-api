import {
  format,
  endOfDay,
  startOfMonth,
  getMonth,
  subMonths,
  subHours,
  lastDayOfMonth,
} from 'date-fns';
import fs from 'fs';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import path from 'path';
import { Op } from 'sequelize';

import Liberacao from '../models/Liberacao';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';

class CreatePdfRelatorioRescisaoService {
  async store(req, res) {
    const { id_contrato } = req.params;

    const contratoEmpresa = await Contrato.findOne({
      where: {
        id: id_contrato,
      },
      include: [
        {
          model: Empresa,
          as: 'dadosEmpresa',
        },
      ],
    });

    if (!contratoEmpresa) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - Rescisão</h3>
      <p>Não há rescisões para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    const {
      ferias_adicionais,
      decimo_terceiro,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      dadosEmpresa,
    } = contratoEmpresa;

    const colaboradores = await Colaborador.findAll({
      where: {
        contrato: id_contrato,
      },
      include: [
        {
          model: Cargo,
          as: 'dadosCargo',
          // attributes: ['nome_cargo'],
        },
      ],
      // raw: true,
    });

    if (!colaboradores.length) {
      return res.status(200).json({
        message: `O contrato não possui colaboradores cadastrados.`,
      });
    }

    let data = [];

    // formatação , .
    const numberToReal = num => {
      try {
        if (num < 0 && num > -0.1) {
          num = 0;
        }
        let numero = num.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
      } catch (error) {
        return error;
      }
    };

    for (let index = 0; index < colaboradores.length; index++) {
      // Retencoes do colaborador
      colaboradores[index].posto = colaboradores[index].dadosCargo.nome_cargo;

      const retencoes = await Retencao.findAll({
        where: {
          id_colaborador: colaboradores[index].id,
          id_contrato,
        },
        raw: true,
      });

      // Liberacoes do colaborador
      const liberacoes = await Liberacao.findAll({
        where: {
          id_contrato,
          id_colaborador: colaboradores[index].id,
          rubrica: 'rescisao',
        },
        raw: true,
      });

      let totalFerias = 0;
      let totalEncargos = 0;
      let total13salario = 0;
      let totalFgts = 0;
      let totalRetido = 0;

      retencoes.forEach(item => {
        totalFerias += item.ferias_adicionais;
        totalEncargos += item.encargos;
        total13salario += item.decimo_terceiro;
        totalFgts += item.multa_fgts;
      });

      totalRetido = totalFerias + totalEncargos + total13salario + totalFgts;

      let totalFeriasLiberado = 0;
      let totalEncargosLiberado = 0;
      let total13salarioLiberado = 0;
      let totalFgtsLiberado = 0;
      let totalLiberado = 0;

      liberacoes.forEach(item => {
        totalFeriasLiberado += item.ferias;
        totalEncargosLiberado += item.encargos;
        total13salarioLiberado += item.decimo_terceiro;
        totalFgtsLiberado += item.multa_fgts;
      });

      totalLiberado =
        totalFeriasLiberado +
        totalEncargosLiberado +
        total13salarioLiberado +
        totalFgtsLiberado;

      // totais
      let saldoFerias = 0;
      let saldoEncargos = 0;
      let saldoFgts = 0;
      let saldo13Salario = 0;
      let saldoTotal = 0;

      saldoFerias = totalFerias - totalFeriasLiberado;
      saldoEncargos = totalEncargos - totalEncargosLiberado;
      saldoFgts = totalFgts - totalFgtsLiberado;
      saldo13Salario = total13salario - total13salarioLiberado;

      saldoTotal = saldoFerias + saldoEncargos + saldoFgts + saldo13Salario;

      colaboradores[index].totalFeriasRetido = totalFerias;
      colaboradores[index].totalEncRetido = totalEncargos;
      colaboradores[index].total13salarioRetido = total13salario;
      colaboradores[index].totalFgtsRetido = totalFgts;
      colaboradores[index].totalRetido = totalRetido;

      colaboradores[index].totalFeriasLiberado = totalFeriasLiberado;
      colaboradores[index].totalEncLiberado = totalEncargosLiberado;
      colaboradores[index].total13salarioLiberado = total13salarioLiberado;
      colaboradores[index].totalFgtsLiberado = totalFgtsLiberado;
      colaboradores[index].totalLiberado = totalLiberado;

      colaboradores[index].saldoFerias = saldoFerias;
      colaboradores[index].saldoEnc = saldoEncargos;
      colaboradores[index].saldo13Salario = saldo13Salario;
      colaboradores[index].saldoFgts = saldoFgts;
      colaboradores[index].saldo = saldoTotal;

      data.push(colaboradores[index]);
    }

    let soma13SalarioRetido = 0;
    let somaFeriasRetido = 0;
    let somaEncargosRetido = 0;
    let somaFgtsRetido = 0;
    let somaTotalRetido = 0;

    let soma13SalarioLiberado = 0;
    let somaFeriasLiberado = 0;
    let somaFgtsLiberado = 0;
    let somaEncargosLiberado = 0;
    let somaTotalLiberado = 0;

    let somaSaldo13Salario = 0;
    let somaSaldoFerias = 0;
    let somaSaldoEncargos = 0;
    let somaSaldoFgts = 0;
    let somaSaldoTotal = 0;

    data.forEach(item => {
      soma13SalarioRetido += item.total13salarioRetido;
      somaFeriasRetido += item.totalFeriasRetido;
      somaEncargosRetido += item.totalEncRetido;
      somaFgtsRetido += item.totalFgtsRetido;
      somaTotalRetido += item.totalRetido;

      soma13SalarioLiberado += item.total13salarioLiberado;
      somaFeriasLiberado += item.totalFeriasLiberado;
      somaEncargosLiberado += item.totalEncLiberado;
      somaFgtsLiberado += item.totalFgtsLiberado;
      somaTotalLiberado += item.totalLiberado;

      somaSaldo13Salario += item.saldo13Salario;
      somaSaldoFerias += item.saldoFerias;
      somaSaldoEncargos += item.saldoEnc;
      somaSaldoFgts += item.saldoFgts;
      somaSaldoTotal += item.saldo;
    });

    soma13SalarioRetido = numberToReal(soma13SalarioRetido);
    somaFeriasRetido = numberToReal(somaFeriasRetido);
    somaEncargosRetido = numberToReal(somaEncargosRetido);
    somaFgtsRetido = numberToReal(somaFgtsRetido);
    somaTotalRetido = numberToReal(somaTotalRetido);

    soma13SalarioLiberado = numberToReal(soma13SalarioLiberado);
    somaFeriasLiberado = numberToReal(somaFeriasLiberado);
    somaFgtsLiberado = numberToReal(somaFgtsLiberado);
    somaEncargosLiberado = numberToReal(somaEncargosLiberado);
    somaTotalLiberado = numberToReal(somaTotalLiberado);

    somaSaldo13Salario = numberToReal(somaSaldo13Salario);
    somaSaldoFerias = numberToReal(somaSaldoFerias);
    somaSaldoEncargos = numberToReal(somaSaldoEncargos);
    somaSaldoFgts = numberToReal(somaSaldoFgts);
    somaSaldoTotal = numberToReal(somaSaldoTotal);

    data.forEach(item => {
      item.totalFeriasRetido = numberToReal(item.totalFeriasRetido);
      item.totalEncRetido = numberToReal(item.totalEncRetido);
      item.total13salarioRetido = numberToReal(item.total13salarioRetido);
      item.totalFgtsRetido = numberToReal(item.totalFgtsRetido);
      item.totalRetido = numberToReal(item.totalRetido);

      item.totalFeriasLiberado = numberToReal(item.totalFeriasLiberado);
      item.totalEncLiberado = numberToReal(item.totalEncLiberado);
      item.total13salarioLiberado = numberToReal(item.total13salarioLiberado);
      item.totalFgtsLiberado = numberToReal(item.totalFgtsLiberado);
      item.totalLiberado = numberToReal(item.totalLiberado);

      item.saldoFerias = numberToReal(item.saldoFerias);
      item.saldoEnc = numberToReal(item.saldoEnc);
      item.saldo13Salario = numberToReal(item.saldo13Salario);
      item.saldoFgts = numberToReal(item.saldoFgts);
      item.saldo = numberToReal(item.saldo);
    });

    if (!data.length) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - Rescisão</h3>
      <p>Não há rescisões para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      console.log(JSON.stringify({ error }, replaceErrors));

      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
        console.log('errorMessages', errorMessages);
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_relatorio-global.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    // coloca a lista em ordem alfabética
    data.sort(function (a, b) {
      let textA = a.name.toUpperCase();
      let textB = b.name.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    // adiciona o índices
    let index = 1;
    data.forEach(item => {
      item.index = index++;
    });

    // objeto para popular o doc
    doc.setData({
      data,

      soma13SalarioRetido,
      somaFeriasRetido,
      somaEncargosRetido,
      somaFgtsRetido,
      somaTotalRetido,

      soma13SalarioLiberado,
      somaFeriasLiberado,
      somaEncargosLiberado,
      somaFgtsLiberado,
      somaTotalLiberado,

      somaSaldo13Salario,
      somaSaldoFerias,
      somaSaldoEncargos,
      somaSaldoFgts,
      somaSaldoTotal,

      empresa: dadosEmpresa.name,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `relatorio-global-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );
      // return res.json(data);
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdfRelatorioRescisaoService();
