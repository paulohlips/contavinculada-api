import Sequelize, { Model } from 'sequelize';

class Contrato extends Model {
  static init(sequelize) {
    super.init(
      {
        empresa: Sequelize.INTEGER,
        fiscal: Sequelize.INTEGER,
        fiscal_substituto: Sequelize.INTEGER,
        descricao_contrato: Sequelize.STRING,
        numero_contrato: Sequelize.STRING,
        processo: Sequelize.STRING,
        conta_vinculada: Sequelize.STRING,
        data_inicio: Sequelize.DATE,
        data_fim: Sequelize.DATE,
        decimo_terceiro: Sequelize.FLOAT,
        ferias_adicionais: Sequelize.FLOAT,
        recisao_fgts: Sequelize.FLOAT,
        encargos: Sequelize.FLOAT,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Empresa, {
      foreignKey: 'empresa',
      as: 'dadosEmpresa',
    });
    this.belongsTo(models.User, {
      foreignKey: 'id',
      as: 'dadosFiscal',
    });
  }
}

export default Contrato;
