import Sequelize from "sequelize";

let json_model = [];
let json_insomnia = [];

const json = {
  competencia: {
    type: Sequelize.DATE,
  },
  colaborador: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  posto: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  remuneracao: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  decimo_terceiro: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  ferias_adicionais: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  multa_fgts: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  encargos: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  total: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
};

let count = 0;

Object.entries(json).forEach(async (item) => {
  let obj = `${Object.keys(json)[count]}: Sequelize.${json.name.type.key}`;
  json_model.push(obj);
  let obj2 = `"${Object.keys(json)[count]}": ""`;
  json_insomnia.push(obj2);
  count++;
});

console.log(json_model);
console.log(json_insomnia);
