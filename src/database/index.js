import Sequelize from 'sequelize';

import User from '../app/models/User';
import Empresa from '../app/models/Empresa';
import Contrato from '../app/models/Contrato';
import Colaborador from '../app/models/Colaborador';
import Cargo from '../app/models/Cargo';
import Historico from '../app/models/Historico';
import PreRetencao from '../app/models/PreRetencao';
import Retencao from '../app/models/Retencao';
import Liberacao from '../app/models/Liberacao';
import Preliberacoes from '../app/models/PreLiberacao';
import databaseConfig from '../config/database';

const models = [
  User,
  Empresa,
  Contrato,
  Colaborador,
  Cargo,
  Historico,
  PreRetencao,
  Retencao,
  Preliberacoes,
  Liberacao,
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
