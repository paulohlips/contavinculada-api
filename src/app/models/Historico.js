import Sequelize, { Model } from 'sequelize';

class Historico extends Model {
  static init(sequelize) {
    super.init(
      {
        id_colaboradores: Sequelize.INTEGER,
        retencao: Sequelize.FLOAT,
        liberacao: Sequelize.FLOAT,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Contrato, {
      foreignKey: 'id_colaborador',
      as: 'colaboradores',
    });
  }
}

export default Historico;
