import Sequelize, { Model } from 'sequelize';

class Liberacao extends Model {
  static init(sequelize) {
    super.init(
      {
        id_contrato: Sequelize.INTEGER,
        id_financeiro: Sequelize.INTEGER,
        id_colaborador: Sequelize.INTEGER,
        id_preliberacao: Sequelize.INTEGER,
        rubrica: Sequelize.STRING,
        colaborador: Sequelize.STRING,
        posto: Sequelize.STRING,
        ano_retirada: Sequelize.STRING,
        inicio_per_aquisitivo: Sequelize.STRING,
        fim_per_aquisitivo: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        decimo_terceiro: Sequelize.FLOAT,
        ferias: Sequelize.FLOAT,
        encargos: Sequelize.FLOAT,
        multa_fgts: Sequelize.FLOAT,
        subtotal: Sequelize.FLOAT,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Contrato, {
      foreignKey: 'id_contrato',
      as: 'dadosContrato',
    });

    this.belongsTo(models.Colaboradores, {
      foreignKey: 'id',
      as: 'dadosColaborador',
    });

    this.belongsTo(models.User, {
      foreignKey: 'id',
      as: 'dadosFinanceiro',
    });
  }
}

export default Liberacao;
