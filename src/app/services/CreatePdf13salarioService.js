import { format } from 'date-fns';
import fs from 'fs';
import Liberacao from '../models/Liberacao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';

let PizZip = require('pizzip');
let Docxtemplater = require('docxtemplater');
let path = require('path');

class CreatePdf13salarioService {
  async store(req, res) {
    const { id_contrato, ano_retirada } = req.params;

    let data = await Liberacao.findAll({
      where: {
        id_contrato,
        ano_retirada,
        rubrica: '13salario',
      },
      raw: true,
    });

    if (data.length === 0) {
      return res.status(200).send(`
    <html>
    <body>

    <h3>Liberação - 13º Salário/${ano_retirada}</h3>
    <p>Não há liberações de 13º Salário no ano retirada selecionado para o contrato informado.</p>
    <p>Selecione outro ano retirada.</p>

    </body>
    </html>`);
    }

    const {
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      dadosEmpresa,
    } = await Contrato.findOne({
      where: { id: data[0].id_contrato },
      include: [
        {
          model: Empresa,
          as: 'dadosEmpresa',
        },
      ],
    });

    // formatação , .
    function numberToReal(numero) {
      try {
        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
      } catch (error) {
        console.log(error);
      }
    }

    // soma totais gerais
    let total_geral = 0;
    let total_13 = 0;
    let total_encargos = 0;

    for (let index = 0; index < data.length; index++) {
      const colaborador = await Colaborador.findOne({
        where: { id: data[index].id_colaborador },
        include: [
          {
            model: Cargo,
            as: 'dadosCargo',
          },
        ],
      });

      data[index].posto = colaborador.dadosCargo.nome_cargo;
    }

    let dataArray = [];

    data.forEach(liberacao => {
      if (!dataArray[`${liberacao.id_colaborador}`]) {
        dataArray[`${liberacao.id_colaborador}`] = {
          id: liberacao.id,
          id_contrato: liberacao.id_contrato,
          id_financeiro: liberacao.id_financeiro,
          id_colaborador: liberacao.id_colaborador,
          id_preliberacao: liberacao.id_preliberacao,
          rubrica: liberacao.rubrica,
          colaborador: liberacao.colaborador,
          posto: liberacao.posto,
          ano_retirada: liberacao.ano_retirada,
          inicio_per_aquisitivo: liberacao.inicio_per_aquisitivo,
          fim_per_aquisitivo: liberacao.fim_per_aquisitivo,
          status: liberacao.status,
          decimo_terceiro: liberacao.decimo_terceiro,
          ferias: liberacao.ferias,
          encargos: liberacao.encargos,
          multa_fgts: liberacao.multa_fgts,
          subtotal: liberacao.subtotal,
          createdAt: liberacao.createdAt,
          updatedAt: liberacao.updatedAt,
        };
      } else if (dataArray[`${liberacao.id_colaborador}`]) {
        dataArray[`${liberacao.id_colaborador}`].subtotal =
          dataArray[`${liberacao.id_colaborador}`].subtotal +
          liberacao.subtotal;
      }
      total_geral += liberacao.subtotal;
      total_13 += liberacao.decimo_terceiro;
      total_encargos += liberacao.encargos;

      liberacao.decimo_terceiro = numberToReal(liberacao.decimo_terceiro);
      liberacao.encargos = numberToReal(liberacao.encargos);
      liberacao.subtotal = numberToReal(liberacao.subtotal);
    });

    dataArray = Object.values(dataArray);
    dataArray.forEach(item => {
      item.decimo_terceiro = numberToReal(item.decimo_terceiro);
      item.encargos = numberToReal(item.encargos);
      item.subtotal = numberToReal(item.subtotal);
    });

    total_13 = numberToReal(total_13);
    total_encargos = numberToReal(total_encargos);
    total_geral = numberToReal(total_geral);

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      console.log(JSON.stringify({ error }, replaceErrors));

      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
        console.log('errorMessages', errorMessages);
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_liberacao-13salario.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    // coloca a lista em ordem alfabética
    dataArray.sort(function (a, b) {
      let textA = a.colaborador.toUpperCase();
      let textB = b.colaborador.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    // adiciona o índices
    let index = 1;
    dataArray.forEach(item => {
      item.index = index++;
    });

    // objeto para popular o doc
    doc.setData({
      data: dataArray,
      total_geral,
      total_13,
      total_encargos,
      empresa: dadosEmpresa.name,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      ano_retirada,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `liberacao-13salario-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );

      // return res.json(dataArray);
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdf13salarioService();
