import * as Yup from 'yup';
import { Op } from 'sequelize';
import { format, startOfMonth, endOfMonth, parseISO, subHours } from 'date-fns';
import pt from 'date-fns/locale/pt-BR';
import paginate from 'paginate-array';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import PreRetencao from '../models/PreRetencao';
import writeLog from '../../utils/logger';

class RetencaoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      id_contrato: Yup.number().required(),
      competencia: Yup.date().required(),
      data: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }
    const { data, competencia, id_contrato } = req.body;

    let parsedCompetencia = parseISO(competencia);

    try {
      const retencoes = data.map(item => ({
        id_financeiro: item.id_financeiro,
        id_colaborador: item.id_colaborador,
        id_contrato,
        competencia: parsedCompetencia,
        colaborador: item.colaborador,
        posto: item.posto,
        remuneracao: item.remuneracao,
        decimo_terceiro: item.decimo13,
        ferias_adicionais: item.ferias,
        encargos: item.encargosSociais,
        multa_fgts: item.fgts,
        total: item.total,
        status: true,
      }));

      retencoes.forEach(async element => {
        const { competencia, id_contrato, id_colaborador } = element;
        let parsedCompetencia = subHours(endOfMonth(competencia), 3);

        const { status, id } = await PreRetencao.findOne({
          where: {
            competencia: {
              [Op.between]: [
                startOfMonth(parsedCompetencia),
                parsedCompetencia,
              ],
            },
            id_contrato,
            id_colaborador,
          },
        });

        if (!status) {
          await PreRetencao.update({ status: true }, { where: { id } });

          element.competencia;
          await Retencao.create(element);
        }
      });

      return res.json({
        message: 'Retenções realizadas com sucesso.',
      });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    try {
      Retencao.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    const { page = 1, number = 5 } = req.query;

    try {
      const retencao = await Retencao.findAll({
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
        order: [['competencia', 'DESC']],
        // limit: number * 12,
        // offset: (page - 1) * number * 12,
      });

      let objData = {};

      retencao.forEach(item => {
        const formattedDate = format(item.competencia, 'MMMM/yyyy', {
          locale: pt,
        });

        const competencia = formattedDate;
        const contrato = item.dadosContrato.numero_contrato;

        if (!objData[`${contrato}-${competencia}`]) {
          objData[`${contrato}-${competencia}`] = {
            id_contrato: item.id_contrato,
            contrato: item.dadosContrato.numero_contrato,
            competencia: formattedDate,
            totalRetido: item.total,
          };
        } else if (
          objData[`${contrato}-${competencia}`] &&
          objData[`${contrato}-${competencia}`].competencia == formattedDate
        ) {
          objData[`${contrato}-${competencia}`].totalRetido =
            objData[`${contrato}-${competencia}`].totalRetido + item.total;
        }
      });

      const data = Object.values(objData);

      data.forEach(element => {
        let total = element.totalRetido;
        element.totalRetido = parseFloat(total.toFixed(2));
      });

      const all = await Retencao.findAll({
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
        order: [['competencia', 'DESC']],
      });

      let objDataAll = {};

      all.forEach(item => {
        const formattedDate = format(item.competencia, 'MMMM/yyyy', {
          locale: pt,
        });

        const competencia = formattedDate;
        const contrato = item.dadosContrato.numero_contrato;

        if (!objDataAll[`${contrato}-${competencia}`]) {
          objDataAll[`${contrato}-${competencia}`] = {
            id_contrato: item.id_contrato,
            contrato: item.dadosContrato.numero_contrato,
            competencia: formattedDate,
            totalRetido: item.total,
          };
        } else if (
          objDataAll[`${contrato}-${competencia}`] &&
          objDataAll[`${contrato}-${competencia}`].competencia == formattedDate
        ) {
          objDataAll[`${contrato}-${competencia}`].totalRetido =
            objDataAll[`${contrato}-${competencia}`].totalRetido + item.total;
        }
      });

      const dataAll = Object.values(objDataAll);

      dataAll.forEach(element => {
        let total = element.totalRetido;
        element.totalRetido = parseFloat(total.toFixed(2));
      });

      const paginateCollection = paginate(data, page, number);

      return res.json({
        pages: paginateCollection.totalPages,
        data: paginateCollection.data,
        dataAll,
      });
    } catch (err) {
      // writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async list(req, res) {
    const tableLength = await Retencao.count();

    const { page = 1, number = 10 } = req.query;

    try {
      const retencao = await Retencao.findAll({
        where: {
          status: false,
        },
        limit: number,
        offset: (page - 1) * number,
        /// /order: [["created_at", "DESC"]],
      });

      let pageNumber = Math.ceil(tableLength / number);

      return res.json({ pages: pageNumber, data: retencao });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new RetencaoController();
