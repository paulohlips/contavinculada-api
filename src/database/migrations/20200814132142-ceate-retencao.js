module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("retencaos", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      id_financeiro: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_contrato: {
        type: Sequelize.INTEGER,
        references: { model: "contratos", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_colaborador: {
        type: Sequelize.INTEGER,
        references: { model: "colaboradores", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      competencia: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      colaborador: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      posto: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      remuneracao: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      decimo_terceiro: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      ferias_adicionais: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      multa_fgts: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      encargos: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      total: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("retencao");
  },
};
