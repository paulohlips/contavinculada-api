import Sequelize, { Model } from 'sequelize';

class Colaboradores extends Model {
  static init(sequelize) {
    super.init(
      {
        contrato: Sequelize.INTEGER,
        empresa: Sequelize.INTEGER,
        name: Sequelize.STRING,
        cargo: Sequelize.INTEGER,
        data_nascimento: Sequelize.DATE,
        telefone: Sequelize.STRING,
        sexo: Sequelize.STRING,
        estado_civil: Sequelize.STRING,
        rg: Sequelize.STRING,
        cpf: Sequelize.STRING,
        data_admissao: Sequelize.DATE,
        data_disponibilizacao: Sequelize.DATE,
        data_desligamento: Sequelize.DATE,
        estado: Sequelize.STRING,
        cidade: Sequelize.STRING,
        bairro: Sequelize.STRING,
        rua: Sequelize.STRING,
        cep: Sequelize.STRING,
        numero: Sequelize.STRING,
        banco: Sequelize.STRING,
        agencia: Sequelize.STRING,
        conta: Sequelize.STRING,
        situacao: Sequelize.STRING,
        motivo: Sequelize.STRING,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Cargo, {
      foreignKey: 'cargo',
      as: 'dadosCargo',
    });

    this.belongsTo(models.Contrato, {
      foreignKey: 'contrato',
      as: 'dadosContrato',
    });

    this.belongsTo(models.Empresa, {
      foreignKey: 'empresa',
      as: 'dadosEmpresa',
    });
  }
}

export default Colaboradores;
