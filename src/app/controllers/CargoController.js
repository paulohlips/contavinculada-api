import * as Yup from 'yup';
import { Op } from 'sequelize';
import Cargo from '../models/Cargo';
import Empresa from '../models/Empresa';

import writeLog from '../../utils/logger';

class CargoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      nome_cargo: Yup.string().required(),
      id_empresa: Yup.number().required(),
      remuneracao: Yup.number().required(),
      descricao: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    try {
      const { nome_cargo, contrato } = req.body;

      const CARGO = nome_cargo.toUpperCase();

      const cargoExists = await Cargo.findOne({
        where: {
          [Op.and]: [{ nome_cargo: CARGO }, { contrato }],
        },
      });

      if (cargoExists) {
        return res.status(422).json({
          message: `A Cargo já está cadastrado no contrato`,
        });
      }

      req.body.nome_cargo = nome_cargo.toUpperCase();

      const cargo = await Cargo.create(req.body);

      return res.json(cargo);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    try {
      Cargo.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    try {
      const { contrato } = req.query;

      if (contrato) {
        const cargos = await Cargo.findAll({
          where: { contrato },
        });

        return res.json(
          cargos.map(item => ({ id: item.id, name: item.nome_cargo })),
        );
      }

      const tableLength = await Cargo.count();

      const { page = 1, number = 10 } = req.query;

      const all = await Cargo.findAll({
        include: [
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name'],
          },
        ],
      });

      const cargos = await Cargo.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
        include: [
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name'],
          },
        ],
      });

      /*       function numberToReal(numero) {
        var numero = numero.toFixed(2).split(".");
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join(".");
        return numero.join(",");
      }

      cargos.forEach((cargo) => {
        cargo.remuneracao = numberToReal(cargo.remuneracao);
      }); */

      let pageNumber = Math.ceil(tableLength / number);

      return res.json({ pages: pageNumber, data: cargos, all });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    try {
      Cargo.destroy({ where: { id } });

      return res.json({ message: 'Operação realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new CargoController();
