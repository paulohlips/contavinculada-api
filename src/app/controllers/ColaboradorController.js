import * as Yup from 'yup';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';

import writeLog from '../../utils/logger';

class ColaboradorController {
  async store(req, res) {
    const schema = Yup.object().shape({
      agencia: Yup.string().nullable(),
      bairro: Yup.string().nullable(),
      banco: Yup.string().nullable(),
      cargo: Yup.number().required(),
      cep: Yup.string().nullable(),
      cidade: Yup.string().nullable(),
      conta: Yup.string().nullable(),
      contrato: Yup.number().required(),
      cpf: Yup.string().required(),
      data_admissao: Yup.date().required(),
      data_desligamento: Yup.date().nullable(),
      data_disponibilizacao: Yup.date().required(),
      data_nascimento: Yup.date().nullable(),
      empresa: Yup.number().required(),
      estado: Yup.string().nullable(),
      estado_civil: Yup.string().nullable(),
      motivo: Yup.string().nullable(),
      name: Yup.string().required(),
      numero: Yup.string().nullable(),
      rg: Yup.string().nullable(),
      rua: Yup.string().nullable(),
      sexo: Yup.string().nullable(),
      situacao: Yup.string().nullable(),
      telefone: Yup.string().nullable(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    try {
      const { cpf } = req.body;

      const colaboradorExists = await Colaborador.findOne({
        where: { cpf },
      });

      if (colaboradorExists) {
        return res
          .status(422)
          .json({ message: 'O colaborador já está cadastrado.' });
      }

      if (req.body.data_disponibilizacao) {
        req.body.situacao = 'ativo';
      }

      const colaborador = await Colaborador.create(req.body);

      return res.json(colaborador);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    /*   if (req.body["data_disponibilizacao"] && req.body["situacao"] ==) {
      req.body["situacao"] = "ativo";
    } */

    try {
      Colaborador.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async index(req, res) {
    try {
      const tableLength = await Colaborador.count();

      const { contrato, situacao } = req.query;

      if (contrato && situacao) {
        const colaboradores = await Colaborador.findAll({
          where: { contrato, situacao },
          include: [
            {
              model: Cargo,
              as: 'dadosCargo',
              attributes: ['id', 'nome_cargo', 'remuneracao'],
            },
            {
              model: Contrato,
              as: 'dadosContrato',
            },
            {
              model: Empresa,
              as: 'dadosEmpresa',
              attributes: ['id', 'name'],
            },
          ],
        });

        colaboradores.sort(function (a, b) {
          let textA = a.name.toUpperCase();
          let textB = b.name.toUpperCase();
          return textA < textB ? -1 : textA > textB ? 1 : 0;
        });

        return res.status(200).json({ colaboradores });
      }

      if (contrato) {
        const all = await Colaborador.findAll({ where: { contrato } });

        const colaboradores = await Colaborador.findAll({
          where: { contrato },
          include: [
            {
              model: Cargo,
              as: 'dadosCargo',
              attributes: ['id', 'nome_cargo', 'remuneracao'],
            },
            {
              model: Contrato,
              as: 'dadosContrato',
            },
            {
              model: Empresa,
              as: 'dadosEmpresa',
              attributes: ['id', 'name'],
            },
          ],
        });

        colaboradores.sort(function (a, b) {
          let textA = a.name.toUpperCase();
          let textB = b.name.toUpperCase();
          return textA < textB ? -1 : textA > textB ? 1 : 0;
        });

        return res.status(200).json({ colaboradores, all });
      }

      const { page = 1, number = 10 } = req.query;
      const all = await Colaborador.findAll({
        include: [
          {
            model: Cargo,
            as: 'dadosCargo',
            attributes: ['id', 'nome_cargo', 'remuneracao'],
          },
          {
            model: Contrato,
            as: 'dadosContrato',
          },
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name'],
          },
        ],
      });

      const colaboradores = await Colaborador.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
        include: [
          {
            model: Cargo,
            as: 'dadosCargo',
            attributes: ['id', 'nome_cargo', 'remuneracao'],
          },
          {
            model: Contrato,
            as: 'dadosContrato',
          },
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name'],
          },
        ],
      });

      let pageNumber = Math.ceil(tableLength / number);

      colaboradores.sort(function (a, b) {
        let textA = a.name.toUpperCase();
        let textB = b.name.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json({ pages: pageNumber, data: colaboradores, all });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async list(req, res) {
    try {
      const colaboradores = await Colaborador.findAll({
        where: {
          situacao: 'ativo',
        },
        include: [
          {
            model: Cargo,
            as: 'dadosCargo',
            attributes: ['id', 'nome_cargo', 'remuneracao'],
          },
          {
            model: Contrato,
            as: 'dadosContrato',
          },
        ],
      });

      colaboradores.sort(function (a, b) {
        let textA = a.name.toUpperCase();
        let textB = b.name.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json(
        colaboradores.map(item => ({ id: item.id, name: item.name })),
      );
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    try {
      Colaborador.destroy({ where: { id } });

      return res.json({ message: 'Operação realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new ColaboradorController();
