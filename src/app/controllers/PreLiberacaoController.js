import {
  parseISO,
  subHours,
  endOfDay,
  lastDayOfYear,
  format,
  getYear,
} from 'date-fns';
import { Op } from 'sequelize';
import * as Yup from 'yup';
import Contrato from '../models/Contrato';
import Preliberacao from '../models/PreLiberacao';
import Retencao from '../models/Retencao';

class PreLiberacao {
  async store(req, res) {
    const schema = Yup.object().shape({
      id_fiscal: Yup.number().required(),
      id_fiscal_substituto: Yup.number().nullable(),
      id_contrato: Yup.number().required(),
      colaboradores: Yup.array().required(),
      inicio_per_aquisitivo: Yup.string().nullable(),
      fim_per_aquisitivo: Yup.string().nullable(),
      ano_retirada: Yup.date().nullable(),
      rubrica: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    let {
      id_contrato,
      id_fiscal,
      id_fiscal_substituto,
      rubrica,
      inicio_per_aquisitivo,
      fim_per_aquisitivo,
      ano_retirada,
      colaboradores,
    } = req.body;

    try {
      const contratoExists = await Contrato.findOne({
        where: { id: id_contrato },
      });

      if (!contratoExists) {
        return res.status(404).json({ message: 'Contrato não encontrado.' });
      }

      if (inicio_per_aquisitivo && fim_per_aquisitivo) {
        req.body.inicio_per_aquisitivo = getYear(
          parseISO(inicio_per_aquisitivo),
        );
        req.body.fim_per_aquisitivo = getYear(parseISO(fim_per_aquisitivo));
      }

      if (ano_retirada) {
        req.body.ano_retirada = getYear(parseISO(ano_retirada));
      }

      let preLibExistFor = [];
      let preRetNotExistFor = [];

      // verifica se há retenção para o colaborador
      for (let index = 0; index < colaboradores.length; index++) {
        let verifyIfExistRetencao = '';
        let start = '';

        // retenções no periodo informado para 13salario ou ferias
        if (rubrica == '13salario' || rubrica == 'ferias') {
          if (ano_retirada) {
            start = new Date(
              format(new Date(`01/01/${ano_retirada}`), 'dd/MM/yyyy'),
            );
          } else {
            start = new Date(
              format(new Date(`01/01/${inicio_per_aquisitivo}`), 'dd/MM/yyyy'),
            );
          }

          const end = subHours(endOfDay(lastDayOfYear(start)), 3);

          verifyIfExistRetencao = await Retencao.findOne({
            where: {
              id_colaborador: colaboradores[index].id_colaborador,
              competencia: {
                [Op.between]: [start, end],
              },
            },
          });
        } else {
          // retenções no caso de rescisão
          verifyIfExistRetencao = await Retencao.findOne({
            where: {
              id_colaborador: colaboradores[index].id_colaborador,
            },
          });
        }

        if (!verifyIfExistRetencao) {
          preRetNotExistFor.push(colaboradores[index].id_colaborador);
        }
      }

      for (let index = 0; index < preRetNotExistFor.length; index++) {
        for (let i = 0; i < colaboradores.length; i++) {
          if (colaboradores[i].id_colaborador == preRetNotExistFor[index])
            colaboradores.splice(i, 1);
        }
      }

      // só permite criação de pre-lib se não houver pré-lib  com status false e se houver saldo
      for (let index = 0; index < colaboradores.length; index++) {
        const verifyIfExistPreRet = await Preliberacao.findOne({
          where: {
            id_contrato,
            rubrica,
            inicio_per_aquisitivo,
            fim_per_aquisitivo,
            ano_retirada,
            id_colaborador: colaboradores[index].id_colaborador,
            status: false,
          },
        });

        if (!verifyIfExistPreRet) {
          await Preliberacao.create({
            id_fiscal,
            id_fiscal_substituto,
            id_contrato,
            id_colaborador: colaboradores[index].id_colaborador,
            nome_colaborador: colaboradores[index].nome_colaborador,
            remuneracao_colaborador:
              colaboradores[index].remuneracao_colaborador,
            rubrica,
            ano_retirada,
            inicio_per_aquisitivo,
            fim_per_aquisitivo,
          });
        } else {
          preLibExistFor.push(colaboradores[index].nome_colaborador);
        }
      }

      if (preLibExistFor.length != 0) {
        return res.json({
          message: `Os colaboradores ${preLibExistFor} já possuem pre-retenção para a rubrica ${rubrica.toUpperCase()} no período informado. As demais pre-liberações foram criadas.`,
        });
      }

      return res.json({ message: 'Pré-Retenções criadas com sucesso' });
    } catch (error) {
      return res.status(500).json({ message: `${error} ` });
    }
  }

  async index(req, res) {
    try {
      const preliberacoes = await Preliberacao.findAll({
        where: { status: false },
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
      });

      return res.json(preliberacoes);
    } catch (error) {
      return res.json({ message: `${error}` });
    }
  }

  async show(req, res) {
    const { ano_retirada, inicio, fim } = req.query;
    const { id_contrato, rubrica } = req.params;

    if (rubrica == 'ferias') {
      try {
        const preliberacoes = await Preliberacao.findAll({
          where: {
            inicio_per_aquisitivo: inicio,
            fim_per_aquisitivo: fim,
            rubrica,
            id_contrato,
          },
        });

        return res.json(preliberacoes);
      } catch (error) {
        return res.json({ message: `${error}` });
      }
    }
  }
}

export default new PreLiberacao();
