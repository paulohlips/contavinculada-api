contavinculada-api

Mudanças:

1) POST /calcretencao 
- Não precisa enviar id da preretenção na url
- No corpo da requisição deve ser enviado competencia e id_contrato
obs: o retorno continua igual.

2) POST /retencao
- Não precisa mais id da preretenção no body
- enviar no body:
{
  "id_contrato",
  "competencia",
  "data": (esse já está sendo enviado, deve apenas adicioanar id_colaborador que agora já está sendo enviada no ato do cálculo da retenção em POST /calcretencao)
}
Dúvidas:
1) Existe um cenário onde o mesmo funcionário tenha duas pré/retenções no mesmo mês?

Migrations:
 - Retencao
 - PreRetencao
 - PreLiberacao