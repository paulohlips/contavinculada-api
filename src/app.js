import express from "express";
import routes from "./routes";
import path from "path";

import "./database";
//import "./scripting";

class App {
  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());

    this.server.use(
      express.static(
        path.join(__dirname, "..", "..", "contavinculada", "build")
      )
    );
  }
  routes() {
    this.server.use(routes);
  }
}

export default new App().server;
