import { Op } from 'sequelize';
import fs from 'fs';
import { format, endOfDay, lastDayOfMonth } from 'date-fns';
import pt from 'date-fns/locale/pt';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import path from 'path';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';

class CreatePdfRetencaoService {
  async store(req, res) {
    const { competencia, id_contrato } = req.params;
    const requestDate = competencia.split('-');
    let requestMonth = parseInt(requestDate[1]);

    /*  if (requestMonth > 12)  */ requestMonth--;

    let comp = new Date(requestDate[0], requestMonth);

    const monthYear = format(comp, 'MMMM/yyyy', {
      locale: pt,
    });

    const lastDayMonth = endOfDay(lastDayOfMonth(comp));

    /* Remember: competencia enviar mês sem zero */

    // const end =

    const data = await Retencao.findAll({
      where: {
        id_contrato,
        competencia: {
          [Op.between]: [comp, lastDayMonth],
        },
      },
      include: [
        {
          model: Contrato,
          as: 'dadosContrato',
        },
      ],
    });

    const formattedDate = format(comp, 'MMMM/yyyy', {
      locale: pt,
    });

    if (!data.length) {
      return res.status(200).send(`

<html>
<body>

<h3>Retenção - ${formattedDate}</h3>
<p>Não há retenções em <b>${formattedDate}</b> para o contrato selecionado.</p>
<p>Selecione outra competência.</p>

</body>
</html>`);
    }

    function numberToReal(numero) {
      var numero = numero.toFixed(2).split('.');
      numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
      return numero.join(',');
    }

    let total_geral = 0;
    let total_13 = 0;
    let total_fgts = 0;
    let total_ferias = 0;
    let total_encargos = 0;
    let total_remuneracao = 0;

    data.forEach(item => {
      total_geral += item.total;
      total_13 += item.decimo_terceiro;
      total_ferias += item.ferias_adicionais;
      total_fgts += item.multa_fgts;
      total_encargos += item.encargos;
      total_remuneracao += item.remuneracao;
    });

    total_geral = numberToReal(total_geral);
    total_13 = numberToReal(total_13);
    total_ferias = numberToReal(total_ferias);
    total_fgts = numberToReal(total_fgts);
    total_encargos = numberToReal(total_encargos);
    total_remuneracao = numberToReal(total_remuneracao);

    data.forEach(item => {
      item.remuneracao = numberToReal(item.remuneracao);
      item.decimo_terceiro = numberToReal(item.decimo_terceiro);
      item.ferias_adicionais = numberToReal(item.ferias_adicionais);
      item.multa_fgts = numberToReal(item.multa_fgts);
      item.encargos = numberToReal(item.encargos);
      item.total = numberToReal(item.total);
    });

    const { name: nameEmpresa } = await Empresa.findOne({
      where: { id: data[0].dadosContrato.empresa },
    });

    const numContaVinculada = data[0].dadosContrato.conta_vinculada;
    const numContrato = data[0].dadosContrato.numero_contrato;
    const numProcesso = data[0].dadosContrato.processo;

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      console.log(JSON.stringify({ error }, replaceErrors));

      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
        console.log('errorMessages', errorMessages);
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_retencao.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    const docDecimoTerceiro = `${data[0].dadosContrato.decimo_terceiro}%`;
    const docFeriasAdicionais = `${data[0].dadosContrato.ferias_adicionais}%`;
    const docRecisaoFgts = `${data[0].dadosContrato.recisao_fgts}%`;
    const docEncargos = `${data[0].dadosContrato.encargos}%`;

    data.sort(function (a, b) {
      let textA = a.colaborador.toUpperCase();
      let textB = b.colaborador.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    let index = 0;

    data.forEach(item => {
      item.index = index + 1;
      index++;
    });

    doc.setData({
      data,
      total_geral,
      total_13,
      total_fgts,
      total_ferias,
      total_encargos,
      total_remuneracao,
      formattedDate,
      nameEmpresa,
      numContaVinculada,
      numContrato,
      numProcesso,
      docDecimoTerceiro,
      docFeriasAdicionais,
      docRecisaoFgts,
      docEncargos,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `retencao-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdfRetencaoService();
