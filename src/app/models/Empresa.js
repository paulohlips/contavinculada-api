import Sequelize, { Model } from 'sequelize';

class Empresa extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        description: Sequelize.STRING,
        cnpj: Sequelize.STRING,
        phone: Sequelize.STRING,
        responsible_name: Sequelize.STRING,
        responsible_email: Sequelize.STRING,
        responsible_phone: Sequelize.STRING,
        status: Sequelize.STRING,
      },
      {
        sequelize,
      },
    );

    return this;
  }
}

export default Empresa;
