module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: 'docker',
  database: 'contavinculada_db',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
    // freezeTableName: true,
  },
};
