import Sequelize, { Model } from 'sequelize';

class Preliberacoes extends Model {
  static init(sequelize) {
    super.init(
      {
        id_fiscal: Sequelize.INTEGER,
        id_fiscal_substituto: Sequelize.INTEGER,
        id_contrato: Sequelize.INTEGER,
        id_colaborador: Sequelize.INTEGER,
        nome_colaborador: Sequelize.STRING,
        remuneracao_colaborador: Sequelize.FLOAT,
        inicio_per_aquisitivo: Sequelize.STRING,
        fim_per_aquisitivo: Sequelize.STRING,
        ano_retirada: Sequelize.STRING,
        rubrica: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Contrato, {
      foreignKey: 'id_contrato',
      as: 'dadosContrato',
    });

    this.belongsTo(models.Colaboradores, {
      foreignKey: 'id_colaborador',
      as: 'dadosColaborador',
    });
  }
}

export default Preliberacoes;
