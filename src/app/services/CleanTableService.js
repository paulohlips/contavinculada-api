import PreRetencao from '../models/PreRetencao';
import Retencao from '../models/Retencao';
import Liberacao from '../models/Liberacao';
import PreLiberacao from '../models/PreLiberacao';

class CleanTaableService {
  async execute(req, res) {
    const { table, comp, id_contrato } = req.params;

    try {
      switch (table) {
        case 'preretencao':
          await PreRetencao.destroy({
            where: {
              competencia: comp,
              id_contrato,
            },
          });
          break;
        case 'retencao':
          await Retencao.destroy({
            where: {
              competencia: comp,
              id_contrato,
            },
          });
          break;
        // case "preliberacao":
        //   await PreLiberacao.destroy()
        //   break;
        // case "liberacao":
        //   await Liberacao.destroy()
        //   break;
        default:
          return res.json(`A tabela ${table} é inválida.`);
      }

      return res.json(`${`${table} ${comp}`}`);
    } catch (err) {
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }
}

export default new CleanTaableService();
