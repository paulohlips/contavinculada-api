import Sequelize, { Model } from 'sequelize';

class Cargo extends Model {
  static init(sequelize) {
    super.init(
      {
        contrato: Sequelize.INTEGER,
        nome_cargo: Sequelize.STRING,
        id_empresa: Sequelize.INTEGER,
        remuneracao: Sequelize.FLOAT,
        descricao: Sequelize.STRING,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Empresa, {
      foreignKey: 'id_empresa',
      as: 'dadosEmpresa',
    });
  }
}

export default Cargo;
