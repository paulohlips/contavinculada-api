module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("contratos", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      id_fiscal: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_fiscal_substituto: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: true,
      },
      empresa: {
        type: Sequelize.INTEGER,
        references: { model: "empresas", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      descricao_contrato: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      numero_contrato: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      processo: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      conta_vinculada: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: false,
      },
      data_inicio: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      data_fim: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      decimo_terceiro: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      ferias_adicionais: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      recisao_fgts: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      encargos: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("contratos");
  },
};
