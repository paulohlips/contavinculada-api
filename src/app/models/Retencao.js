import Sequelize, { Model } from 'sequelize';

class Retencao extends Model {
  static init(sequelize) {
    super.init(
      {
        id_financeiro: Sequelize.INTEGER,
        id_contrato: Sequelize.INTEGER,
        id_colaborador: Sequelize.INTEGER,
        competencia: Sequelize.DATE,
        colaborador: Sequelize.STRING,
        posto: Sequelize.STRING,
        remuneracao: Sequelize.FLOAT,
        decimo_terceiro: Sequelize.FLOAT,
        ferias_adicionais: Sequelize.FLOAT,
        multa_fgts: Sequelize.FLOAT,
        encargos: Sequelize.FLOAT,
        total: Sequelize.FLOAT,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'id_financeiro',
      as: 'users',
    });

    this.belongsTo(models.Contrato, {
      foreignKey: 'id_contrato',
      as: 'dadosContrato',
    });
  }
}

export default Retencao;
