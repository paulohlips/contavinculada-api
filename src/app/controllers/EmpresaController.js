import * as Yup from 'yup';
import Empresa from '../models/Empresa';
import writeLog from '../../utils/logger';

class EmpresaController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      description: Yup.string(),
      cnpj: Yup.string().required(),
      phone: Yup.string().required(),
      responsible_name: Yup.string().required(),
      responsible_email: Yup.string().required(),
      responsible_phone: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    try {
      const { cnpj } = req.body;

      const empresaExists = await Empresa.findOne({ where: { cnpj } });

      if (empresaExists) {
        return res
          .status(422)
          .json({ message: 'A empresa já está cadastrado.' });
      }

      const empresa = await Empresa.create(req.body);

      return res.json(empresa);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    try {
      const empresaTableLength = await Empresa.count();

      const { page = 1, number = 10 } = req.query;
      const empresa = await Empresa.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
      });

      const all = await Empresa.findAll();

      let pageNumber = Math.ceil(empresaTableLength / number);

      return res.json({ pages: pageNumber, data: empresa, all });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async list(req, res) {
    try {
      const empresas = await Empresa.findAll({
        attributes: ['id', 'name'],
      });

      return res.json(empresas);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;

    try {
      Empresa.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    try {
      Empresa.destroy({ where: { id } });

      return res.json({ message: 'Operação realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new EmpresaController();
