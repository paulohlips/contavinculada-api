import { Op } from 'sequelize';
import { startOfMonth, endOfMonth } from 'date-fns';
import * as Yup from 'yup';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import writeLog from '../../utils/logger';
//
class ContratoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      empresa: Yup.number().required(),
      fiscal: Yup.number().required(),
      fiscal_substituto: Yup.number(),
      descricao_contrato: Yup.string().required(),
      numero_contrato: Yup.string().required(),
      data_inicio: Yup.string().required(),
      data_fim: Yup.string().required(),
      conta_vinculada: Yup.string().required(),
      processo: Yup.string().required(),
      decimo_terceiro: Yup.number(),
      ferias_adicionais: Yup.number(),
      recisao_fgts: Yup.number(),
      encargos: Yup.number(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    try {
      const { numero_contrato, empresa } = req.body;

      const contratoExists = await Contrato.findOne({
        where: { numero_contrato },
      });

      if (contratoExists) {
        return res
          .status(422)
          .json({ message: 'O contrato já está cadastrado.' });
      }

      const empresaExists = await Empresa.findOne({
        where: { id: empresa },
      });

      if (!empresaExists) {
        return res.status(422).json({ message: 'Empresa não cadastrada.' });
      }

      const contrato = await Contrato.create(req.body);

      return res.json(contrato);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async index(req, res) {
    try {
      const { empresa } = req.query;

      const all = await Contrato.findAll({
        include: [
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name', 'cnpj'],
          },
        ],
      });

      if (empresa) {
        try {
          const all = await Contrato.findAll({
            where: { empresa },
            include: [
              {
                model: Empresa,
                as: 'dadosEmpresa',
                attributes: ['id', 'name', 'cnpj'],
              },
            ],
          });

          const contratos = await Contrato.findAll({
            where: { empresa },
          });

          return res.json({ contratos, all });
        } catch (err) {
          writeLog(err);
          return res.status(500).json({ message: `Erro no servidor. ${err}` });
        }
      }

      const tableLength = await Contrato.count();

      const { page = 1, number = 10 } = req.query;
      const contratos = await Contrato.findAll({
        limit: number,
        offset: (page - 1) * number,
        order: [['created_at', 'DESC']],
        include: [
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name', 'cnpj'],
          },
        ],
      });

      let pageNumber = Math.ceil(tableLength / number);

      return res.json({ pages: pageNumber, data: contratos, all });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async list(req, res) {
    const { empresa, competencia } = req.query;

    if (empresa) {
      try {
        const contratos = await Contrato.findAll({
          attributes: [
            'id',
            'numero_contrato',
            'decimo_terceiro',
            'ferias_adicionais',
            'recisao_fgts',
            'encargos',
            'descricao_contrato',
            'conta_vinculada',
            'processo',
          ],
          include: [
            {
              model: Empresa,
              as: 'dadosEmpresa',
              attributes: ['id', 'name', 'cnpj'],
            },
          ],
          where: { empresa },
        });

        return res.json(
          contratos.map(contrato => ({
            id: contrato.id,
            name: contrato.id,
            decimo_terceiro: contrato.decimo_terceiro,
            ferias_adicionais: contrato.ferias_adicionais,
            recisao_fgts: contrato.recisao_fgts,
            encargos: contrato.encargos,
          })),
        );
      } catch (err) {
        writeLog(err);
        return res.status(500).json({ message: `Erro no servidor. ${err}` });
      }
    }

    // if (competencia) {
    //   let comp = new Date(competencia);

    //   const listacontratos = await Contrato.findAll({
    //     attributes: [
    //       "id",
    //       "numero_contrato",
    //       "decimo_terceiro",
    //       "ferias_adicionais",
    //       "recisao_fgts",
    //       "encargos",
    //       "descricao_contrato",
    //       "conta_vinculada",
    //       "processo",
    //     ],
    //     include: [
    //       {
    //         model: Empresa,
    //         as: "dadosEmpresa",
    //         attributes: ["id", "name", "cnpj"],
    //       },
    //     ],
    //   });

    //   const preRetencao = await PreRetencao.findAll({
    //     where: {
    //       competencia: {
    //         [Op.between]: [startOfMonth(comp), endOfMonth(comp)],
    //       },
    //     },
    //   });

    //   /*
    //   Modificar filtro: atualmente selecionando um colaborador todo o cotrato some.
    //   Comportamento esperado: sumir com o colaborador que tem retenção.
    //   */

    //   const ids = preRetencao.map((item) => {
    //     return item.id_contrato;
    //   });

    //   let contratos = [];

    //   listacontratos.forEach((contrato) => {
    //     if (!ids.includes(contrato.id)) {
    //       contratos.push(contrato);
    //     }
    //   });

    //   return res.json(contratos);
    // }

    try {
      const contratos = await Contrato.findAll({
        attributes: [
          'id',
          'numero_contrato',
          'decimo_terceiro',
          'ferias_adicionais',
          'recisao_fgts',
          'encargos',
          'descricao_contrato',
          'conta_vinculada',
          'processo',
        ],
        include: [
          {
            model: Empresa,
            as: 'dadosEmpresa',
            attributes: ['id', 'name', 'cnpj'],
          },
        ],
      });

      return res.json(contratos);
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }

  async update(req, res) {
    const { id } = req.params;
    try {
      Contrato.update(req.body, { where: { id } });

      return res.json({ message: 'Atualização realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    try {
      Contrato.destroy({ where: { id } });

      return res.json({ message: 'Operação realizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new ContratoController();
