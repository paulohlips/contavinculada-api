import {
  format,
  endOfDay,
  startOfMonth,
  getMonth,
  subMonths,
  subHours,
  lastDayOfMonth,
} from 'date-fns';
import fs from 'fs';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import path from 'path';
import { Op } from 'sequelize';

import Liberacao from '../models/Liberacao';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';

class CreatePdfRelatorioFeriasService {
  async store(req, res) {
    const {
      id_contrato,
      inicio_per_aquisitivo,
      fim_per_aquisitivo,
    } = req.params;

    const contratoEmpresa = await Contrato.findOne({
      where: {
        id: id_contrato,
      },
      include: [
        {
          model: Empresa,
          as: 'dadosEmpresa',
        },
      ],
    });

    if (!contratoEmpresa) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - Férias ${inicio_per_aquisitivo}/${fim_per_aquisitivo}</h3>
      <p>Não há relatório de férias para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    const {
      ferias_adicionais,
      decimo_terceiro,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      dadosEmpresa,
    } = contratoEmpresa;

    const colaboradores = await Colaborador.findAll({
      where: {
        contrato: id_contrato,
      },
      include: [
        {
          model: Cargo,
          as: 'dadosCargo',
          // attributes: ['nome_cargo'],
        },
      ],
      // raw: true,
    });

    if (!colaboradores.length) {
      return res.status(200).json({
        message: `O contrato não possui colaboradores cadastrados.`,
      });
    }

    let data = [];

    // formatação , .
    const numberToReal = num => {
      try {
        if (num < 0 && num > -0.1) {
          num = 0;
        }
        let numero = num.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
      } catch (error) {
        return error;
      }
    };

    for (let index = 0; index < colaboradores.length; index++) {
      // Retencoes do colaborador
      colaboradores[index].posto = colaboradores[index].dadosCargo.nome_cargo;

      const dayMonth = format(
        new Date(colaboradores[index].data_admissao),
        'MM/dd',
      );

      let start = new Date(
        format(
          new Date(`${`${inicio_per_aquisitivo}/${dayMonth}`}`),
          'yyyy/MM/dd',
        ),
      );

      let end = new Date(
        format(
          new Date(`${`${fim_per_aquisitivo}/${dayMonth}`}`),
          'yyyy/MM/dd',
        ),
      );

      // end = subDays(end, 1);
      start = startOfMonth(start);

      const monthDataDisp = getMonth(colaboradores[index].data_admissao);

      const lastFebDay = format(
        new Date(`${colaboradores[index].data_admissao}`),
        'dd',
      );

      if (monthDataDisp == 1 && lastFebDay == '29') {
        start = subMonths(start, 1);
        end = subHours(endOfDay(lastDayOfMonth(subMonths(end, 2))), 3);
      } else {
        end = subHours(endOfDay(lastDayOfMonth(subMonths(end, 1))), 3);
      }

      const retencoes = await Retencao.findAll({
        where: {
          id_colaborador: colaboradores[index].id,
          id_contrato,
          competencia: {
            [Op.between]: [start, end],
          },
        },
        raw: true,
      });

      // Liberacoes do colaborador
      const liberacoes = await Liberacao.findAll({
        where: {
          id_contrato,
          id_colaborador: colaboradores[index].id,
          inicio_per_aquisitivo,
          fim_per_aquisitivo,
          rubrica: 'ferias',
        },
        raw: true,
      });

      // totais
      let saldoFerias = 0;
      let saldoEncFerias = 0;
      let saldoTotal = 0;

      let totalFerias = 0;
      let totalEncFerias = 0;
      let totalRetido = 0;

      retencoes.forEach(item => {
        totalFerias += item.ferias_adicionais;
        totalEncFerias += item.encargos;
      });

      let totalFeriasLiberado = 0;
      let totalEncFeriasliberado = 0;

      liberacoes.forEach(item => {
        totalFeriasLiberado += item.ferias;
        totalEncFeriasliberado += item.encargos;
      });

      totalEncFerias -=
        totalEncFerias *
        (decimo_terceiro / (ferias_adicionais + decimo_terceiro));

      // totalEncFerias = parseFloat(totalEncFerias.toFixed(2));

      saldoFerias = totalFerias - totalFeriasLiberado;
      saldoEncFerias = totalEncFerias - totalEncFeriasliberado;
      saldoTotal = saldoFerias + saldoEncFerias;

      // totalRetido = totalFerias + totalEncFerias;
      // totalRetido = parseFloat(totalRetido.toFixed(2));

      colaboradores[index].totalFeriasRetido = totalFerias;
      colaboradores[index].totalEncFeriasRetido = totalEncFerias;
      colaboradores[index].totalRetido = totalFerias + totalEncFerias;

      colaboradores[index].totalFeriasLiberado = totalFeriasLiberado;
      colaboradores[index].totalEncFeriasLiberado = totalEncFeriasliberado;
      colaboradores[index].totalLiberado =
        totalFeriasLiberado + totalEncFeriasliberado;

      colaboradores[index].saldoFerias = saldoFerias;
      colaboradores[index].saldoEncargosRetido = saldoEncFerias;
      colaboradores[index].total = saldoTotal;

      data.push(colaboradores[index]);
    }

    let somaFeriasliberado = 0;
    let somaEncargosLiberado = 0;
    let somaFeriasRetido = 0;
    let somaEncargosRetido = 0;
    let totalLiberado = 0;
    let totalRetido = 0;
    let somaSaldoFerias = 0;
    let somaSaldoEncargos = 0;
    let somaSaldoTotal = 0;

    data.forEach(item => {
      somaFeriasliberado += item.totalFeriasLiberado;
      somaEncargosLiberado += item.totalEncFeriasLiberado;
      somaFeriasRetido += item.totalFeriasRetido;
      somaEncargosRetido += item.totalEncFeriasRetido;
    });

    somaSaldoFerias = somaFeriasRetido - somaFeriasliberado;
    somaSaldoEncargos = somaEncargosRetido - somaEncargosLiberado;
    somaSaldoTotal = somaSaldoFerias + somaSaldoEncargos;
    totalRetido = somaFeriasRetido + somaEncargosRetido;
    somaSaldoTotal = somaSaldoFerias + somaSaldoEncargos;

    somaFeriasliberado = numberToReal(somaFeriasliberado);
    somaEncargosLiberado = numberToReal(somaEncargosLiberado);
    somaFeriasRetido = numberToReal(somaFeriasRetido);
    somaEncargosRetido = numberToReal(somaEncargosRetido);
    somaSaldoFerias = numberToReal(somaSaldoFerias);
    somaSaldoEncargos = numberToReal(somaSaldoEncargos);
    totalRetido = numberToReal(totalRetido);
    totalLiberado = numberToReal(totalLiberado);
    somaSaldoTotal = numberToReal(somaSaldoTotal);

    data.forEach(item => {
      item.totalFeriasRetido = numberToReal(item.totalFeriasRetido);
      item.totalEncFeriasRetido = numberToReal(item.totalEncFeriasRetido);
      item.totalRetido = numberToReal(item.totalRetido);

      item.totalFeriasLiberado = numberToReal(item.totalFeriasLiberado);
      item.totalEncFeriasLiberado = numberToReal(item.totalEncFeriasLiberado);
      item.totalLiberado = numberToReal(item.totalLiberado);

      item.saldoFerias = numberToReal(item.saldoFerias);
      item.saldoEncargosRetido = numberToReal(item.saldoEncargosRetido);
      item.total = numberToReal(item.total);
    });

    if (!data.length) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - Férias ${inicio_per_aquisitivo}/${fim_per_aquisitivo}</h3>
      <p>Não há relatório de férias para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      console.log(JSON.stringify({ error }, replaceErrors));

      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
        console.log('errorMessages', errorMessages);
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_relatorio-ferias.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    // coloca a lista em ordem alfabética
    data.sort(function (a, b) {
      let textA = a.name.toUpperCase();
      let textB = b.name.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    // adiciona o índices
    let index = 1;
    data.forEach(item => {
      item.index = index++;
    });

    // objeto para popular o doc
    doc.setData({
      data,
      somaFeriasliberado,
      somaEncargosLiberado,
      somaFeriasRetido,
      somaEncargosRetido,
      somaSaldoFerias,
      somaSaldoEncargos,
      somaSaldoTotal,
      totalLiberado,
      totalRetido,
      empresa: dadosEmpresa.name,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      inicio_per_aquisitivo,
      fim_per_aquisitivo,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `relatorio-ferias-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );
      // return res.json(data);
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdfRelatorioFeriasService();
