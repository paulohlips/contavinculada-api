import {
  parseISO,
  getYear,
  lastDayOfYear,
  format,
  subHours,
  subMonths,
  lastDayOfMonth,
  startOfMonth,
  endOfDay,
  getMonth,
} from 'date-fns';
import { Op } from 'sequelize';
import * as Yup from 'yup';

import PreLiberacao from '../models/PreLiberacao';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';
import Liberacao from '../models/Liberacao';

class CalcLiberacao {
  async index(req, res) {
    const schema = Yup.object().shape({
      id_contrato: Yup.number().required(),
      rubrica: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    let {
      id_contrato,
      rubrica,
      inicio_per_aquisitivo,
      fim_per_aquisitivo,
      ano_retirada,
    } = req.body;

    inicio_per_aquisitivo = getYear(parseISO(inicio_per_aquisitivo)).toString();
    fim_per_aquisitivo = getYear(parseISO(fim_per_aquisitivo)).toString();

    const contratoExists = await Contrato.findOne({
      where: { id: id_contrato },
    });

    if (!contratoExists) {
      return res.status(404).json({ message: 'Contrato não encontrado.' });
    }

    // try {
    let preLiberacoes = [];
    let liberacoes = [];
    let subtotal_no_periodo = 0;
    let total_ferias_retido = 0;
    let total_13salario_retido = 0;
    let total_13salario_retido_no_periodo = 0;
    let total_encargos_retido_no_periodo = 0;
    let total_ferias_retido_no_periodo = 0;
    let colaborador = '';
    let posto = '';
    let ferias = 0;
    let fgts = 0;
    let total_geral = 0;
    let total_encargos_retido = 0;
    let total_encargos_liberado = 0;
    let total_ferias_liberado = 0;
    let total_fgts_liberado = 0;
    let total_13salario_liberado = 0;
    let total_13salario = 0;
    let total_encargos = 0;
    let total_ferias = 0;
    let total_fgts = 0;
    let encargos = 0;
    let liberado_13salario_no_periodo = 0;
    let liberado_encargos_no_periodo = 0;
    let total_ferias_liberado_periodo = 0;
    let total_encargos_liberado_periodo = 0;

    if (rubrica === 'ferias') {
      preLiberacoes = await PreLiberacao.findAll({
        where: {
          id_contrato,
          rubrica,
          inicio_per_aquisitivo,
          fim_per_aquisitivo,
          status: false,
        },
      });
    } else if (rubrica === '13salario') {
      preLiberacoes = await PreLiberacao.findAll({
        where: {
          id_contrato,
          rubrica,
          ano_retirada,
          status: false,
        },
      });
    } else if (rubrica === 'rescisao') {
      preLiberacoes = await PreLiberacao.findAll({
        where: {
          id_contrato,
          rubrica,
          status: false,
        },
      });
    } else {
      return res
        .status(404)
        .json({ message: `A rubrica '${rubrica}' é inválida.` });
    }

    for (let index = 0; index < preLiberacoes.length; index++) {
      switch (rubrica) {
        case 'ferias':
          try {
            const {
              id: id_colaborador,
              data_admissao,
              data_disponibilizacao,
              dadosCargo,
            } = await Colaborador.findOne({
              where: { id: preLiberacoes[index].id_colaborador },
              include: [
                {
                  model: Cargo,
                  as: 'dadosCargo',
                  attributes: ['id', 'nome_cargo', 'remuneracao'],
                },
              ],
            });

            const dayMonth = format(new Date(data_admissao), 'MM/dd');

            let start = new Date(
              format(
                new Date(`${`${inicio_per_aquisitivo}/${dayMonth}`}`),
                'yyyy/MM/dd',
              ),
            );

            let end = new Date(
              format(
                new Date(`${`${fim_per_aquisitivo}/${dayMonth}`}`),
                'yyyy/MM/dd',
              ),
            );

            // end = subDays(end, 1);
            start = startOfMonth(start);

            const monthDataDisp = getMonth(data_admissao);

            const lastFebDay = format(new Date(`${data_admissao}`), 'dd');

            if (monthDataDisp == 1 && lastFebDay == '29') {
              start = subMonths(start, 1);
              end = subHours(endOfDay(lastDayOfMonth(subMonths(end, 2))), 3);
            } else {
              end = subHours(endOfDay(lastDayOfMonth(subMonths(end, 1))), 3);
            }

            const retencao = await Retencao.findAll({
              where: {
                id_colaborador,
                id_contrato,
                competencia: {
                  [Op.between]: [start, end],
                },
              },
              raw: true,
            });

            const retencaoTotal = await Retencao.findAll({
              where: {
                id_colaborador,
                id_contrato,
              },
              raw: true,
            });

            const liberacao = await Liberacao.findAll({
              where: {
                id_colaborador,
                inicio_per_aquisitivo,
                fim_per_aquisitivo,
                rubrica,
              },
              raw: true,
            });

            const liberacaoTotal = await Liberacao.findAll({
              where: {
                id_colaborador,
                id_contrato,
                rubrica,
              },
              raw: true,
            });

            subtotal_no_periodo = 0;

            total_ferias_retido = 0;
            total_ferias_liberado = 0;
            total_ferias_liberado_periodo = 0;
            total_ferias_retido_no_periodo = 0;

            total_encargos_liberado = 0;
            total_encargos_retido = 0;
            total_encargos_retido_no_periodo = 0;
            total_encargos_liberado_periodo = 0;

            colaborador = preLiberacoes[index].nome_colaborador;

            ferias = 0;
            fgts = 0;
            encargos = 0;
            total_geral = 0;

            if (retencaoTotal.length) {
              retencaoTotal.forEach(retencao => {
                total_ferias_retido += retencao.ferias_adicionais;
                total_encargos_retido += retencao.encargos;
              });
            }

            if (liberacaoTotal.length) {
              liberacaoTotal.forEach(liberacao => {
                total_ferias_liberado += liberacao.ferias;
                total_encargos_liberado += liberacao.encargos;
              });
            }

            if (liberacao.length) {
              liberacao.forEach(liberacao => {
                total_ferias_liberado_periodo += liberacao.ferias;
                total_encargos_liberado_periodo += liberacao.encargos;
              });
            }

            if (retencao.length) {
              retencao.forEach(retencao => {
                total_ferias_retido_no_periodo += retencao.ferias_adicionais;
                total_encargos_retido_no_periodo += retencao.encargos;
              });

              // calcula o proporcional de encargos de férias retido
              total_encargos_retido -=
                (total_encargos_retido * contratoExists.decimo_terceiro) /
                (contratoExists.ferias_adicionais +
                  contratoExists.decimo_terceiro);

              // calcula o proporcional de encargos de férias retido no período
              total_encargos_retido_no_periodo -=
                (total_encargos_retido_no_periodo *
                  contratoExists.decimo_terceiro) /
                (contratoExists.ferias_adicionais +
                  contratoExists.decimo_terceiro);

              total_ferias_retido -= total_ferias_liberado;
              total_encargos_retido -= total_encargos_liberado;

              let saldoFerias = total_ferias_retido;
              let saldoEncargos = total_encargos_retido;

              let subtotal_saldo = saldoFerias + saldoEncargos;

              saldoFerias = parseFloat(saldoFerias.toFixed(2));
              saldoEncargos = parseFloat(saldoEncargos.toFixed(2));
              total_ferias_retido = parseFloat(total_ferias_retido.toFixed(2));

              total_ferias_retido_no_periodo -= total_ferias_liberado_periodo;
              total_encargos_retido_no_periodo -= total_encargos_liberado_periodo;

              subtotal_no_periodo =
                total_ferias_retido_no_periodo +
                total_encargos_retido_no_periodo;

              total_encargos_retido_no_periodo = parseFloat(
                total_encargos_retido_no_periodo.toFixed(2),
              );

              subtotal_no_periodo = parseFloat(subtotal_no_periodo.toFixed(2));

              total_ferias_retido_no_periodo = parseFloat(
                total_ferias_retido_no_periodo.toFixed(2),
              );
              subtotal_saldo = parseFloat(subtotal_saldo.toFixed(2));
              total_encargos_retido = parseFloat(
                total_encargos_retido.toFixed(2),
              );

              liberacoes.push({
                id_colaborador,
                id_preliberacao: preLiberacoes[index].id,
                colaborador,
                posto: dadosCargo.nome_cargo,
                inicio_per_aquisitivo,
                fim_per_aquisitivo,
                // total_ferias_retido,
                // total_encargos_retido,
                total_ferias_retido_no_periodo,
                total_encargos_retido_no_periodo,
                subtotal_no_periodo,
                saldoFerias,
                saldoEncargos,
                subtotal_saldo,
              });
            }
          } catch (error) {
            return res.status(500).json({ message: error });
          }

          break;
        case '13salario':
          try {
            const {
              id: id_colaborador,
              name,
              dadosCargo,
            } = await Colaborador.findOne({
              where: { id: preLiberacoes[index].id_colaborador },
              include: [
                {
                  model: Cargo,
                  as: 'dadosCargo',
                  attributes: ['id', 'nome_cargo', 'remuneracao'],
                },
              ],
            });
            const start = new Date(
              format(new Date(`01/01/${ano_retirada}`), 'dd/MM/yyyy'),
            );

            const end = subHours(endOfDay(lastDayOfYear(start)), 3);

            liberado_13salario_no_periodo = 0;
            liberado_encargos_no_periodo = 0;
            subtotal_no_periodo = 0;
            total_ferias_retido = 0;
            total_13salario_retido = 0;
            total_13salario_retido_no_periodo = 0;
            total_encargos_retido_no_periodo = 0;
            total_encargos_retido = 0;
            total_13salario_liberado = 0;
            total_encargos_liberado = 0;
            total_ferias_retido_no_periodo = 0;
            ferias = 0;
            fgts = 0;
            encargos = 0;
            total_geral = 0;

            const retencao = await Retencao.findAll({
              where: {
                id_colaborador,
                id_contrato,
                competencia: {
                  [Op.between]: [start, end],
                },
              },
              raw: true,
            });

            const retencaoTotal = await Retencao.findAll({
              where: {
                id_colaborador,
                id_contrato,
              },
              raw: true,
            });

            const liberacao = await Liberacao.findAll({
              where: {
                id_colaborador,
                ano_retirada,
                id_contrato,
              },
              raw: true,
            });

            liberacao.forEach(lib => {
              (liberado_13salario_no_periodo += lib.decimo_terceiro),
                (liberado_encargos_no_periodo += lib.encargos);
            });

            if (retencaoTotal.length) {
              retencaoTotal.forEach(retencao => {
                total_13salario_retido += retencao.decimo_terceiro;
                total_encargos_retido += retencao.encargos;
              });
            }

            const liberacaoTotal = await Liberacao.findAll({
              where: {
                id_colaborador,
                rubrica: '13salario',
                id_contrato,
              },
              raw: true,
            });

            if (liberacaoTotal.length) {
              liberacaoTotal.forEach(liberacao => {
                total_13salario_liberado += liberacao.decimo_terceiro;
                total_encargos_liberado += liberacao.encargos;
              });
            }

            // Total retido de 13º para o período
            if (retencao.length) {
              retencao.forEach(retencao => {
                total_13salario_retido_no_periodo += retencao.decimo_terceiro;
                total_encargos_retido_no_periodo += retencao.encargos;
              });

              // calcula o proporcional de encargos de férias retido
              total_encargos_retido -=
                (total_encargos_retido * contratoExists.ferias_adicionais) /
                (contratoExists.ferias_adicionais +
                  contratoExists.decimo_terceiro);

              total_encargos_retido_no_periodo -=
                (total_encargos_retido_no_periodo *
                  contratoExists.ferias_adicionais) /
                (contratoExists.ferias_adicionais +
                  contratoExists.decimo_terceiro);

              let saldo13salario =
                total_13salario_retido - total_13salario_retido_no_periodo;
              let saldoEncargos =
                total_encargos_retido - total_encargos_retido_no_periodo;
              let subtotal_saldo = saldo13salario + saldoEncargos;

              subtotal_no_periodo =
                total_13salario_retido_no_periodo +
                total_encargos_retido_no_periodo -
                liberado_13salario_no_periodo -
                liberado_encargos_no_periodo;

              // Totais gerais e totais no ano referência"
              total_13salario_retido -= total_13salario_liberado;
              total_encargos_retido -= total_encargos_liberado;
              total_13salario_retido_no_periodo -= liberado_13salario_no_periodo;
              total_encargos_retido_no_periodo -= liberado_encargos_no_periodo;

              total_13salario_retido = parseFloat(
                total_13salario_retido.toFixed(2),
              );
              total_encargos_retido = parseFloat(
                total_encargos_retido.toFixed(2),
              );
              total_encargos_retido_no_periodo = parseFloat(
                total_encargos_retido_no_periodo.toFixed(2),
              );
              subtotal_no_periodo = parseFloat(subtotal_no_periodo.toFixed(2));
              saldo13salario = parseFloat(saldo13salario.toFixed(2));
              saldoEncargos = parseFloat(saldoEncargos.toFixed(2));
              total_13salario_retido_no_periodo = parseFloat(
                total_13salario_retido_no_periodo.toFixed(2),
              );
              total_encargos_retido = parseFloat(
                total_encargos_retido.toFixed(2),
              );
              subtotal_saldo = parseFloat(subtotal_saldo.toFixed(2));

              liberacoes.push({
                id_colaborador,
                id_preliberacao: preLiberacoes[index].id,
                colaborador: name,
                posto: dadosCargo.nome_cargo,
                ano_retirada,
                total_13salario_retido,
                total_13salario_retido_no_periodo,
                total_encargos_retido,
                total_encargos_retido_no_periodo,
                subtotal_no_periodo,
                saldo13salario,
                saldoEncargos,
                subtotal_saldo,
              });
            }
          } catch (error) {
            return res.status(500).json({ message: error });
          }
          break;
        case 'rescisao':
          try {
            const {
              id: id_colaborador,
              name,
              dadosCargo,
              dadosContrato,
            } = await Colaborador.findOne({
              where: { id: preLiberacoes[index].id_colaborador },
              include: [
                {
                  model: Cargo,
                  as: 'dadosCargo',
                  attributes: ['id', 'nome_cargo', 'remuneracao'],
                },
                {
                  model: Contrato,
                  as: 'dadosContrato',
                  attributes: ['decimo_terceiro', 'ferias_adicionais'],
                },
              ],
            });

            subtotal_no_periodo = 0;
            total_ferias_retido = 0;
            total_13salario_retido = 0;
            total_13salario_retido_no_periodo = 0;
            total_encargos_retido_no_periodo = 0;
            total_ferias_retido_no_periodo = 0;
            colaborador = 0;
            posto = 0;
            ferias = 0;
            fgts = 0;
            encargos = 0;
            total_geral = 0;
            total_fgts = 0;
            total_encargos = 0;
            total_13salario = 0;
            total_ferias = 0;
            total_encargos_liberado = 0;
            total_ferias_liberado = 0;
            total_13salario_liberado = 0;
            total_fgts_liberado = 0;

            const retencao = await Retencao.findAll({
              where: {
                id_colaborador,
                id_contrato,
              },
              raw: true,
            });

            if (retencao.length) {
              retencao.forEach(retencao => {
                total_13salario += retencao.decimo_terceiro;
                total_ferias += retencao.ferias_adicionais;
                total_fgts += retencao.multa_fgts;
                total_encargos += retencao.encargos;
              });

              const liberacaoTotal = await Liberacao.findAll({
                where: {
                  id_colaborador,
                  id_contrato,
                },
                raw: true,
              });

              if (liberacaoTotal.length) {
                liberacaoTotal.forEach(liberacao => {
                  total_13salario_liberado += liberacao.decimo_terceiro;
                  total_ferias_liberado += liberacao.ferias;
                  total_encargos_liberado += liberacao.encargos;
                  total_fgts_liberado += liberacao.multa_fgts;
                });
              }

              let atual_13salario = total_13salario - total_13salario_liberado;
              let atual_ferias = total_ferias - total_ferias_liberado;
              let atual_fgts = total_fgts - total_fgts_liberado;
              let atual_encargos = total_encargos - total_encargos_liberado;

              total_geral =
                atual_13salario + atual_ferias + atual_fgts + atual_encargos;

              let atual_encargo_13salario =
                atual_encargos *
                (1 -
                  dadosContrato.ferias_adicionais /
                    (dadosContrato.ferias_adicionais +
                      dadosContrato.decimo_terceiro));
              let atual_encargo_ferias =
                atual_encargos *
                (1 -
                  dadosContrato.decimo_terceiro /
                    (dadosContrato.ferias_adicionais +
                      dadosContrato.decimo_terceiro));

              atual_13salario = parseFloat(atual_13salario.toFixed(2));
              atual_encargos = parseFloat(atual_encargos.toFixed(2));
              total_geral = parseFloat(total_geral.toFixed(2));
              atual_ferias = parseFloat(atual_ferias.toFixed(2));
              atual_fgts = parseFloat(atual_fgts.toFixed(2));
              atual_encargo_13salario = parseFloat(
                atual_encargo_13salario.toFixed(2),
              );
              atual_encargo_ferias = parseFloat(
                atual_encargo_ferias.toFixed(2),
              );

              liberacoes.push({
                id_colaborador,
                id_preliberacao: preLiberacoes[index].id,
                colaborador: name,
                posto: dadosCargo.nome_cargo,
                atual_13salario,
                atual_ferias,
                atual_fgts,
                atual_encargos,
                atual_encargo_13salario,
                atual_encargo_ferias,
                total_geral,
              });
            }

            break;
          } catch (error) {
            return res.status(500).json({ message: error });
          }
        default:
          liberacoes = `Rubrica ${rubrica} não é válida`;
          break;
      }
    }

    liberacoes.sort((a, b) => {
      const textA = a.colaborador.toUpperCase();
      const textB = b.colaborador.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    return res.json(liberacoes);
  }

  catch(err) {
    return res.status(500).json({ message: `${err}` });
  }
}

export default new CalcLiberacao();
