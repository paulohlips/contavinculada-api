module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("preliberacoes", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      id_fiscal: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_fiscal_substituto: {
        type: Sequelize.INTEGER,
        references: { model: "users", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: true,
      },
      id_contrato: {
        type: Sequelize.INTEGER,
        references: { model: "contratos", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      id_colaborador: {
        type: Sequelize.INTEGER,
        references: { model: "colaboradores", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      nome_colaborador: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      remuneracao_colaborador: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      ano_retirada: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      inicio_per_aquisitivo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      fim_per_aquisitivo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      rubrica: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("preliberacoes");
  },
};
