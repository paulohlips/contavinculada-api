import * as Yup from 'yup';
import bcrypt from 'bcrypt';

import User from '../models/User';
import writeLog from '../../utils/logger';

class RecoveryPassword {
  async update(req, res) {
    const schema = Yup.object().shape({
      password: Yup.string().required(),
      email: Yup.string().required(),
      matricula: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }
    const { email, password, matricula, new_password } = req.body;

    const user = await User.findOne({
      where: {
        email,
      },
    });

    if (!user) {
      return res.status(404).json({ message: 'Usuário não encontrado.' });
    }

    if (user.matricula !== matricula) {
      return res.status(422).json({
        message: 'Dados incorretos.',
      });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(422).json({ message: 'Senha incorreta.' });
    }

    const newPass = await bcrypt.hash(new_password, 8);

    try {
      await User.update(
        { password_hash: newPass, new_user: false },
        { where: { email } },
      );

      return res.json({ message: 'Senha atualizada com sucesso.' });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `Erro no servidor. ${err}` });
    }
  }
}

export default new RecoveryPassword();
