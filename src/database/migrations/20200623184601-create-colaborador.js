"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("colaboradores", {
      id: {
        type: Sequelize.INTEGER,

        autoIncrement: true,
        primaryKey: true,
      },
      empresa: {
        type: Sequelize.INTEGER,
        references: { model: "empresas", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      contrato: {
        type: Sequelize.INTEGER,
        references: { model: "contratos", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      cargo: {
        type: Sequelize.INTEGER,
        references: { model: "cargos", key: "id" },
        onUpdate: "SET NULL",
        onDelete: "SET NULL",
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      telefone: {
        type: Sequelize.STRING,
      },
      data_nascimento: {
        type: Sequelize.DATE,
      },
      sexo: {
        type: Sequelize.STRING,
      },
      estado_civil: {
        type: Sequelize.STRING,
      },

      rg: {
        type: Sequelize.STRING,
      },
      cpf: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      data_admissao: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      data_disponibilizacao: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      data_desligamento: {
        type: Sequelize.DATE,
      },
      estado: {
        type: Sequelize.STRING,
      },
      cidade: {
        type: Sequelize.STRING,
      },
      bairro: {
        type: Sequelize.STRING,
      },
      rua: {
        type: Sequelize.STRING,
      },
      cep: {
        type: Sequelize.STRING,
      },
      numero: {
        type: Sequelize.STRING,
      },

      banco: {
        type: Sequelize.STRING,
      },
      agencia: {
        type: Sequelize.STRING,
      },
      conta: {
        type: Sequelize.STRING,
      },

      situacao: {
        type: Sequelize.STRING,
        defaultValue: "inativo",
      },
      motivo: {
        type: Sequelize.STRING,
        defaultValue: "",
      },

      created_at: {
        type: Sequelize.DATE,
      },
      updated_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('colaboradores');
    */
  },
};
