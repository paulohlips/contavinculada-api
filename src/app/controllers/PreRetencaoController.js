import {
  format,
  parseISO,
  startOfMonth,
  endOfMonth,
  isBefore,
  isAfter,
  differenceInCalendarDays,
  subHours,
} from 'date-fns';
import pt from 'date-fns/locale/pt';

import { Op } from 'sequelize';

import * as Yup from 'yup';
import PreRetencoes from '../models/PreRetencao';
import Contrato from '../models/Contrato';

class PreRetencaoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      id_fiscal: Yup.number().required(),
      id_fiscal_substituto: Yup.number().nullable(),
      id_contrato: Yup.number().required(),
      colaboradores: Yup.array().required(),
      competencia: Yup.date().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    const {
      id_fiscal,
      id_fiscal_substituto,
      id_contrato,
      competencia,
      colaboradores,
    } = req.body;

    try {
      const contratoExists = await Contrato.findOne({
        where: { id: id_contrato },
      });

      if (!contratoExists) {
        return res.status(404).json({ message: 'Contrato não encontrado.' });
      }

      const comp = parseISO(competencia);

      // Verifica idade do contrato
      const contractOlderEnough = differenceInCalendarDays(
        comp,
        contratoExists.data_inicio,
      );

      const formattedDate = format(comp, 'dd/MMMM/yyyy', {
        locale: pt,
      });

      // -15 dias cadastra a pre-retenção com mensagem de "tem menos de 15 dias"
      // if (contractOlderEnough < 15 && contractOlderEnough > 0) {
      //   //await PreRetencoes.create(req.body);

      //   let preRetExistFor = [];

      //   for (let index = 0; index < colaboradores.length; index++) {
      //     const verifyIfExistPreRet = await PreRetencoes.findOne({
      //       where: {
      //         id_contrato,
      //         competencia,
      //         id_colaborador: colaboradores[index].id,
      //         nome_colaborador: colaboradores[index].name,
      //       },
      //     });

      //     if (!verifyIfExistPreRet) {
      //       await PreRetencoes.create({
      //         id_fiscal,
      //         id_fiscal_substituto,
      //         id_contrato,
      //         competencia,
      //         id_colaborador: colaboradores[index].id,
      //         nome_colaborador: colaboradores[index].name,
      //         remuneracao_colaborador: colaboradores[index].remuneracao,
      //       });
      //     } else {
      //       preRetExistFor.push(colaboradores[index].name);
      //     }
      //   }

      //   return res.status(200).json({
      //     message: `Não há retenção de férias/13º/encargos;
      //               Há retenção de multa do FGTS (proporcional aos dias trabalhados).`,
      //   });
      // }

      if (!isBefore(contratoExists.data_inicio, comp)) {
        return res.status(422).json({
          message:
            'A competência não pode ser anterior à data de início da vigência do contrato.',
        });
      }

      const endOfContract = subHours(endOfMonth(contratoExists.data_fim), 3);

      if (!isAfter(endOfContract, comp)) {
        return res.status(422).json({
          message: 'A retenção não pode ser posterior à vigência do contrato.',
        });
      }

      let preRetExistFor = [];

      for (let index = 0; index < colaboradores.length; index++) {
        const verifyIfExistPreRet = await PreRetencoes.findOne({
          where: {
            id_contrato,
            competencia,
            id_colaborador: colaboradores[index].id,
            nome_colaborador: colaboradores[index].name,
          },
        });

        if (!verifyIfExistPreRet) {
          await PreRetencoes.create({
            id_fiscal,
            id_fiscal_substituto,
            id_contrato,
            competencia,
            id_colaborador: colaboradores[index].id,
            nome_colaborador: colaboradores[index].name,
            remuneracao_colaborador: colaboradores[index].remuneracao,
          });
        } else {
          preRetExistFor.push(colaboradores[index].name);
        }
      }

      if (preRetExistFor.length != 0) {
        return res.json({
          message: `Os colaboradores ${preRetExistFor} já possuem pre-retenção para a competencia ${formattedDate}. As demais pre-retenções foram criadas.`,
        });
      }
      return res.json({ message: 'Pré-Retenções criadas com sucesso' });
    } catch (error) {
      return res.status(500).json({ message: `${error}` });
    }
  }

  async index(req, res) {
    try {
      const preretencoes = await PreRetencoes.findAll({
        where: { status: false /* id_fiscal: req.userId */ },
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
      });

      preretencoes.sort(function (a, b) {
        let textA = a.nome_colaborador.toUpperCase();
        let textB = b.nome_colaborador.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json(preretencoes);
    } catch (error) {
      return res.json({ message: `${error}` });
    }
  }

  async show(req, res) {
    const { competencia } = req.query;

    if (!competencia) {
      return res.status(422).json({
        message: 'Período de competência inválido',
      });
    }

    let acompetencia = new Date(competencia);

    try {
      let preretencoes = await PreRetencoes.findAll({
        where: {
          competencia: {
            [Op.between]: [
              startOfMonth(acompetencia),
              endOfMonth(acompetencia),
            ],
          },
          status: false,
        },
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: ['numero_contrato'],
          },
        ],
      });

      preretencoes = preretencoes.map(item => {
        return {
          id_contrato: item.id_contrato,
          numero_contrato: item.dadosContrato.numero_contrato,
        };
      });

      let numContratos = preretencoes.filter(
        (v, i, a) => a.indexOf(v, i) === i,
      );

      let contratos = Array.from(new Set(numContratos.map(JSON.stringify))).map(
        JSON.parse,
      );

      return res.json({ contratos });
    } catch (error) {
      return res.json({ message: `${error}` });
    }
  }
}

export default new PreRetencaoController();
