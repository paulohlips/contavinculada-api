import * as Yup from 'yup';
import { Op } from 'sequelize';
import {
  differenceInCalendarDays,
  lastDayOfMonth,
  getDate,
  endOfDay,
  subHours,
  format,
} from 'date-fns';
import Colaborador from '../models/Colaborador';
import Contrato from '../models/Contrato';
import PreRetencao from '../models/PreRetencao';
import Cargo from '../models/Cargo';

class CalcRetencao {
  async index(req, res) {
    const schema = Yup.object().shape({
      id_contrato: Yup.number().required(),
      competencia: Yup.date().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    const { competencia, id_contrato } = req.body;

    let comp = lastDayOfMonth(new Date(competencia));

    const monthYear = format(new Date(competencia), 'yyyy/MM');
    const lastDay = subHours(endOfDay(lastDayOfMonth(comp)), 3);

    let start = new Date(
      format(new Date(`${`${monthYear}/` + `01`}`), 'yyyy/MM/dd'),
    );

    let retencao = [];
    let colaborador = {};

    try {
      const preretencoes = await PreRetencao.findAll({
        where: {
          id_contrato,
          status: false,
          competencia: {
            [Op.between]: [start, lastDay],
          },
        },
        include: [
          {
            model: Contrato,
            as: 'dadosContrato',
            attributes: [
              'decimo_terceiro',
              'ferias_adicionais',
              'recisao_fgts',
              'encargos',
              'data_inicio',
            ],
          },
        ],
      });

      for (let index = 0; index < preretencoes.length; index++) {
        colaborador = await Colaborador.findOne({
          where: { id: preretencoes[index].id_colaborador },
          include: [
            {
              model: Cargo,
              as: 'dadosCargo',
              attributes: ['id', 'nome_cargo', 'remuneracao'],
            },
          ],
        });

        const {
          data_inicio,
          decimo_terceiro,
          ferias_adicionais,
          recisao_fgts,
          encargos,
        } = preretencoes[index].dadosContrato;

        const { id, name, dadosCargo, data_disponibilizacao } = colaborador;

        let { remuneracao } = dadosCargo;
        let decimo13 = 0;
        let ferias = 0;
        let encargosSociais = 0;
        let fgts = 0;
        let total = 0;
        let totalFloat = 0;
        let message = '';

        const lastMonthDay = getDate(lastDayOfMonth(comp));

        let contratoOlderEnough = differenceInCalendarDays(comp, data_inicio);

        contratoOlderEnough++;

        let colaboradorOlderEnough = differenceInCalendarDays(
          comp,
          data_disponibilizacao,
        );

        colaboradorOlderEnough++;

        // ainda não trabalhou na competencia
        if (colaboradorOlderEnough <= 0) {
          total = 0;
          message = 'Não possui retenção.';
        }

        // contrato tem menos de 15 dias na competência? só tem fgts
        // else if (contratoOlderEnough < 15) {
        //   fgts =
        //     (((remuneracao * colaboradorOlderEnough) / lastMonthDay) *
        //       recisao_fgts) /
        //     100;
        //   total = fgts;
        //   fgts = parseFloat(fgts.toFixed(2));
        //   total = parseFloat(total.toFixed(2));
        //   totalFloat = total;

        //   message = `Trabalhou ${colaboradorOlderEnough}`;
        // }

        // colaborador trabalhou de 15 a 31 dias
        else if (colaboradorOlderEnough >= 1) {
          // if (colaboradorOlderEnough >= contratoOlderEnough) {
          //   colaboradorOlderEnough = contratoOlderEnough;
          // }

          // if (colaboradorOlderEnough > lastMonthDay) {
          //   colaboradorOlderEnough = lastMonthDay;
          // }

          decimo13 = (remuneracao * decimo_terceiro) / 100;
          ferias = (remuneracao * ferias_adicionais) / 100;

          encargosSociais = (remuneracao * encargos) / 100;
          fgts =
            (remuneracao *
              ((recisao_fgts * lastMonthDay) /* colaboradorOlderEnough */ /
                lastMonthDay)) /
            100;
          total = decimo13 + ferias + encargosSociais + fgts;

          decimo13 = parseFloat(decimo13.toFixed(2));
          ferias = parseFloat(ferias.toFixed(2));
          encargosSociais = parseFloat(encargosSociais.toFixed(2));
          fgts = parseFloat(fgts.toFixed(2));
          total = parseFloat(total.toFixed(2));
          totalFloat = total;

          message = `Trabalhou ${colaboradorOlderEnough} dias`;
        }

        retencao.push({
          id: index + 1,
          id_colaborador: id,
          id_contrato,
          id_financeiro: req.userId,
          competencia,
          colaborador: name,
          posto: dadosCargo.nome_cargo,
          remuneracao,
          decimo13,
          ferias,
          encargosSociais,
          fgts,
          total,
          totalFloat,
          message,
        });
      }

      retencao.sort(function (a, b) {
        let textA = a.colaborador.toUpperCase();
        let textB = b.colaborador.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      return res.json(retencao);
    } catch (error) {
      return res.json({ message: `${error}` });
    }
  }
}

export default new CalcRetencao();
