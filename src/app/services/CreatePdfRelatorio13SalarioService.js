import { format, endOfDay, lastDayOfYear, addDays, subHours } from 'date-fns';
import fs from 'fs';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import path from 'path';
import { Op } from 'sequelize';

import Liberacao from '../models/Liberacao';
import Retencao from '../models/Retencao';
import Contrato from '../models/Contrato';
import Empresa from '../models/Empresa';
import Colaborador from '../models/Colaborador';
import Cargo from '../models/Cargo';

class CreatePdfRelatorio13salarioService {
  async store(req, res) {
    const { id_contrato, ano_retirada } = req.params;

    const contratoEmpresa = await Contrato.findOne({
      where: {
        id: id_contrato,
      },
      include: [
        {
          model: Empresa,
          as: 'dadosEmpresa',
        },
      ],
    });

    if (!contratoEmpresa) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - 13º Salário ${ano_retirada}</h3>
      <p>Não há relatório de 13º Salário para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    const {
      ferias_adicionais,
      decimo_terceiro,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      dadosEmpresa,
    } = contratoEmpresa;

    const colaboradores = await Colaborador.findAll({
      where: {
        contrato: id_contrato,
      },
      include: [
        {
          model: Cargo,
          as: 'dadosCargo',
          // attributes: ['nome_cargo'],
        },
      ],
      // raw: true,
    });

    if (!colaboradores.length) {
      return res.status(200).json({
        message: `O contrato não possui colaboradores cadastrados.`,
      });
    }

    let data = [];

    // formatação , .
    const numberToReal = num => {
      try {
        if (num < 0 && num > -0.1) {
          num = 0;
        }
        let numero = num.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
      } catch (error) {
        return error;
      }
    };

    for (let index = 0; index < colaboradores.length; index++) {
      // Retencoes do colaborador
      const comp = new Date(ano_retirada);
      const lastDayYear = subHours(
        endOfDay(lastDayOfYear(addDays(comp, 1))),
        3,
      );

      colaboradores[index].posto = colaboradores[index].dadosCargo.nome_cargo;

      const retencoes = await Retencao.findAll({
        where: {
          id_contrato,
          id_colaborador: colaboradores[index].id,
          competencia: {
            [Op.between]: [comp, lastDayYear],
          },
        },
        raw: true,
      });

      // Liberacoes do colaborador
      const liberacoes = await Liberacao.findAll({
        where: {
          id_contrato,
          id_colaborador: colaboradores[index].id,
          ano_retirada,
          rubrica: '13salario',
        },
        raw: true,
      });

      // totais
      let saldo13salario = 0;
      let saldoEnc13salario = 0;
      let saldoTotal = 0;

      let total13retido = 0;
      let totalEnc13retido = 0;
      let totalRetido = 0;

      retencoes.forEach(item => {
        total13retido += item.decimo_terceiro;
        totalEnc13retido += item.encargos;
      });

      let total13liberado = 0;
      let totalEnc13liberado = 0;

      liberacoes.forEach(item => {
        total13liberado += item.decimo_terceiro;
        totalEnc13liberado += item.encargos;
      });

      totalEnc13retido -=
        totalEnc13retido *
        (ferias_adicionais / (ferias_adicionais + decimo_terceiro));

      saldo13salario = total13retido - total13liberado;
      saldoEnc13salario = totalEnc13retido - totalEnc13liberado;
      saldoTotal = saldo13salario + saldoEnc13salario;

      totalRetido = total13retido + totalEnc13retido;

      colaboradores[index].total13salarioRetido = total13retido;
      colaboradores[index].totalEnc13salarioRetido = totalEnc13retido;
      colaboradores[index].totalRetido = total13retido + totalEnc13retido;

      colaboradores[index].total13salarioLiberado = total13liberado;
      colaboradores[index].totalEnc13salarioLiberado = totalEnc13liberado;
      colaboradores[index].total13Liberado =
        total13liberado + totalEnc13liberado;

      colaboradores[index].saldo13salario = saldo13salario;
      colaboradores[index].saldoEncargosRetido = saldoEnc13salario;
      colaboradores[index].total = saldoTotal;

      data.push(colaboradores[index]);
    }

    let soma13liberado = 0;
    let somaEncargosLiberado = 0;
    let soma13retido = 0;
    let somaEncargosRetido = 0;
    let totalLiberado = 0;
    let totalRetido = 0;
    let somaSaldo13 = 0;
    let somaSaldoEncargos = 0;
    let somaSaldoTotal = 0;

    data.forEach(item => {
      soma13liberado += item.total13salarioLiberado;
      somaEncargosLiberado += item.totalEnc13salarioLiberado;

      soma13retido += item.total13salarioRetido;
      somaEncargosRetido += item.totalEnc13salarioRetido;
    });

    totalRetido = soma13retido + somaEncargosRetido;
    totalLiberado = soma13liberado + somaEncargosLiberado;

    somaSaldo13 = soma13retido - soma13liberado;
    somaSaldoEncargos = somaEncargosRetido - somaEncargosLiberado;
    somaSaldoTotal = somaSaldo13 + somaSaldoEncargos;

    soma13liberado = numberToReal(soma13liberado);
    somaEncargosLiberado = numberToReal(somaEncargosLiberado);
    soma13retido = numberToReal(soma13retido);
    somaEncargosRetido = numberToReal(somaEncargosRetido);
    somaSaldo13 = numberToReal(somaSaldo13);
    somaSaldoEncargos = numberToReal(somaSaldoEncargos);
    totalRetido = numberToReal(totalRetido);
    totalLiberado = numberToReal(totalLiberado);
    somaSaldoTotal = numberToReal(somaSaldoTotal);

    data.forEach(item => {
      item.total13salarioRetido = numberToReal(item.total13salarioRetido);
      item.totalEnc13salarioRetido = numberToReal(item.totalEnc13salarioRetido);
      item.totalRetido = numberToReal(item.totalRetido);

      item.total13salarioLiberado = numberToReal(item.total13salarioLiberado);
      item.totalEnc13salarioLiberado = numberToReal(
        item.totalEnc13salarioLiberado,
      );
      item.total13Liberado = numberToReal(item.total13Liberado);

      item.saldo13salario = numberToReal(item.saldo13salario);
      item.saldoEncargosRetido = numberToReal(item.saldoEncargosRetido);
      item.total = numberToReal(item.total);
    });

    if (!data.length) {
      return res.status(200).send(`
      <html>
      <body>

      <h3>Relatório - 13º Salário ${ano_retirada}</h3>
      <p>Não há relatório de 13º Salário para o contrato informado.</p>
      <p>Selecione outro contrato.</p>

      </body>
      </html>`);
    }

    function replaceErrors(key, value) {
      if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
          error[key] = value[key];
          return error;
        }, {});
      }
      return value;
    }

    function errorHandler(error) {
      if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors
          .map(function (error) {
            return error.properties.explanation;
          })
          .join('\n');
      }
      throw error;
    }

    let content = fs.readFileSync(
      path.resolve(
        __dirname,
        '../../../static/templates/template_relatorio-13salario.docx',
      ),
      'binary',
    );

    let zip = new PizZip(content);
    let doc;

    try {
      doc = new Docxtemplater(zip);
    } catch (error) {
      errorHandler(error);
    }

    // coloca a lista em ordem alfabética
    data.sort(function (a, b) {
      let textA = a.name.toUpperCase();
      let textB = b.name.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });

    // adiciona o índices
    let index = 1;
    data.forEach(item => {
      item.index = index++;
    });

    // objeto para popular o doc
    doc.setData({
      data,
      soma13liberado,
      somaEncargosLiberado,
      soma13retido,
      somaEncargosRetido,
      somaSaldo13,
      somaSaldoEncargos,
      somaSaldoTotal,
      totalLiberado,
      totalRetido,
      empresa: dadosEmpresa.name,
      conta_vinculada,
      numero_contrato,
      processo,
      descricao_contrato,
      ano_retirada,
    });

    fs.readdir(
      path.resolve(__dirname, `../../../static/docs`),
      (err, files) => {
        files.forEach(file => {
          fs.unlink(
            path.resolve(__dirname, `../../../static/docs/${file}`),
            callback => {},
          );
        });
      },
    );

    try {
      doc.render();
    } catch (error) {
      errorHandler(error);
    }

    let buf = doc.getZip().generate({ type: 'nodebuffer' });

    const date = format(new Date(), 'dd-MM-yyyy-HH:mm:ss');

    const docName = `relatorio-13salario-${date}.docx`;

    fs.writeFileSync(
      path.resolve(__dirname, `../../../static/docs/${docName}`),
      buf,
    );

    try {
      return res.download(
        path.resolve(__dirname, '..', '..', '..', 'static', 'docs', docName),
      );
    } catch (error) {
      return res.json({ message: error });
    }
  }
}

export default new CreatePdfRelatorio13salarioService();
