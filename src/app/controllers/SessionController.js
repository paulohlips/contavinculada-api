import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import writeLog from '../../utils/logger';

import User from '../models/User';

import auth from '../../config/auth';

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      password: Yup.string().required(),
      email: Yup.string().email(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(422).json({
        message: 'Erro de validação. Verifique o body da requisição.',
      });
    }

    const { email, password } = req.body;

    try {
      const user = await User.findOne({
        where: {
          email,
        },
      });

      if (!user) {
        return res.status(404).json({ message: 'Usuário não encontrado.' });
      }

      if (!(await user.checkPassword(password))) {
        return res
          .status(401)
          .json({ message: 'A senha informada está incorreta.' });
      }

      const { id, name, type, new_user, register } = user;

      // sessionLogger(id, name, email, register);

      return res.json({
        user: {
          id,
          name,
          type,
          email,
          new_user,
        },
        token: jwt.sign({ id, type }, auth.secret, {
          expiresIn: auth.expiresIn,
        }),
      });
    } catch (err) {
      writeLog(err);
      return res.status(500).json({ message: `${err}` });
    }
  }
}

export default new SessionController();
