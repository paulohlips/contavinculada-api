import { Router } from 'express';
import cors from 'cors';

import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import EmpresaController from './app/controllers/EmpresaController';
import ContratoController from './app/controllers/ContratoController';
import ColaboradorController from './app/controllers/ColaboradorController';
import CargoController from './app/controllers/CargoController';
import PreRetencaoController from './app/controllers/PreRetencaoController';
import LiberacaoController from './app/controllers/LiberacaoController';
import RetencaoController from './app/controllers/RetencaoController';
import RecoveyPassword from './app/services/RecoveyPassword';
import CalcRetencaoService from './app/services/CalcRetencaoService';
import CreatePdfRetencaoService from './app/services/CreatePdfRetencaoService';
import CalcLiberacaoService from './app/services/CalcLiberacaoService';
import PreliberacaoController from './app/controllers/PreLiberacaoController';
// import CleanTableService from './app/services/CleanTableService';
import CreatePdfRescisaoService from './app/services/CreatePdfRescisaoService';
import CreatePdfFeriasService from './app/services/CreatePdfFeriasService';
import CreatePdf13salarioService from './app/services/CreatePdf13salarioService';
import CreatePdfRelatorio13salarioService from './app/services/CreatePdfRelatorio13SalarioService';
import CreatePdfRelatorioFeriasService from './app/services/CreatePdfRelatorioFeriasService';
import CreatePdfRelatorioRescisaoService from './app/services/CreatePdfRelatorioRescisaoService';
import authMiddleware from './app/middlewares/auth';

const routes = new Router();

routes.use(cors());

/* routes.get("/", (req, res) => {
  return res.sendFile(
    "/home/paulo/contavinculada-api/contavinculada/build/index.html"
  );
}); */

routes.post('/sessions', SessionController.store);
routes.get('/users', UserController.index);

routes.get(
  '/docretencao/:competencia/:id_contrato',
  CreatePdfRetencaoService.store,
);

routes.get('/docrescisao/:id_contrato', CreatePdfRescisaoService.store);

routes.get(
  '/docferias/:id_contrato/:inicio/:fim',
  CreatePdfFeriasService.store,
);
routes.get(
  '/doc13salario/:id_contrato/:ano_retirada',
  CreatePdf13salarioService.store,
);

routes.get(
  '/relatorio13salario/:id_contrato/:ano_retirada',
  CreatePdfRelatorio13salarioService.store,
);

routes.get(
  '/relatorioferias/:id_contrato/:inicio_per_aquisitivo/:fim_per_aquisitivo',
  CreatePdfRelatorioFeriasService.store,
);

routes.get(
  '/relatoriorescisao/:id_contrato',
  CreatePdfRelatorioRescisaoService.store,
);

// routes.get('/clean/:table/:comp/:id_contrato', CleanTableService.execute);
routes.put('/recoverypass', RecoveyPassword.update);

routes.use(authMiddleware);

routes.post('/users', UserController.store);
routes.get('/users/:type', UserController.list);
routes.put('/users/:id', UserController.update);

routes.get('/empresas', EmpresaController.index);
routes.get('/listEmpresas', EmpresaController.list);
routes.post('/empresas', EmpresaController.store);
routes.put('/empresas/:id', EmpresaController.update);
routes.delete('/empresas/:id', EmpresaController.delete);

routes.post('/contratos', ContratoController.store);
routes.get('/contratos', ContratoController.index);
routes.get('/listContratos', ContratoController.list);
routes.put('/contratos/:id', ContratoController.update);
routes.delete('/contratos/:id', ContratoController.delete);

routes.post('/colaboradores', ColaboradorController.store);
routes.get('/colaboradores', ColaboradorController.index);
routes.get('/listColaboradores', ColaboradorController.list);
routes.put('/colaboradores/:id', ColaboradorController.update);
routes.delete('/colaboradores/:id', ColaboradorController.delete);

routes.post('/cargos', CargoController.store);
routes.get('/cargos', CargoController.index);
routes.put('/cargos/:id', CargoController.update);
routes.delete('/cargos/:id', CargoController.delete);

routes.post('/calcretencao', CalcRetencaoService.index);

routes.post('/retencao', RetencaoController.store);
routes.get('/retencao', RetencaoController.index);
routes.get('/listRetencao', RetencaoController.list);
routes.put('/retencao/:id', RetencaoController.update);

routes.post('/calcliberacao', CalcLiberacaoService.index);

routes.get('/liberacao/:id_contrato/:rubrica', LiberacaoController.list);
routes.get('/listLiberacao', LiberacaoController.index);
routes.post('/liberacao', LiberacaoController.store);
routes.post('/cancel', LiberacaoController.cancel);
routes.put('/liberacao/:id', LiberacaoController.update);

routes.get('/preretencao', PreRetencaoController.index);
routes.get('/listpreretencao', PreRetencaoController.show);
routes.post('/preretencao', PreRetencaoController.store);

routes.get('/preliberacao', PreliberacaoController.index);
routes.get(
  '/listpreliberacao/:id_contrato/:rubrica',
  PreliberacaoController.show,
);

routes.post('/preliberacao', PreliberacaoController.store);

export default routes;
